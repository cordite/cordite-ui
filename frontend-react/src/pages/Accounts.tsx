import React, { Dispatch, Fragment, useEffect } from "react";
import {
  makeStyles,
  Theme,
  createStyles,
  createMuiTheme,
  ThemeProvider,
  Grid,
} from "@material-ui/core";
import { RouteComponentProps } from "react-router-dom";
import { blueGrey } from "@material-ui/core/colors";
import { useDispatch } from "react-redux";
import {
  getAccountBalance,
  IBraidAccountActions,
  IBraidAccountBalanceActions,
  updateAccount,
} from "../store/actions/braidActions";
import TransferMoney from "../components/accounts/TransferMoney";
import Transactions from "../components/accounts/Transactions";
import AccountBalance from "../components/accounts/AccountBalanceComp";

type TParams = { id?: string };
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    toolbar: {
      display: "flex",
      alignItems: "center",
      justifyContent: "flex-end",
      padding: theme.spacing(0, 1),
      // necessary for content to be below app bar
      ...theme.mixins.toolbar,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
  })
);
let theme = createMuiTheme({
  palette: {
    primary: { main: blueGrey[600] },
  },
});
const Accounts = ({ match }: RouteComponentProps<TParams>) => {
  const classes = useStyles();
  const accountBalanceDispatch = useDispatch<
    Dispatch<IBraidAccountBalanceActions>
  >();
  //lifecycle methods
  const accountDispatch = useDispatch<Dispatch<IBraidAccountActions>>();
  useEffect(() => {
    accountDispatch(updateAccount());
    accountBalanceDispatch(getAccountBalance(match.params.id));
  }, [match.params.id, accountDispatch, accountBalanceDispatch]);
  return (
    <Fragment>
      <ThemeProvider theme={theme}>
        <main className={classes.content}>
          <div className={classes.toolbar} />
          <Grid container>
            <TransferMoney />
            <AccountBalance id={match.params.id} />
            <Transactions id={match.params.id} />
          </Grid>
        </main>
      </ThemeProvider>
    </Fragment>
  );
};
export default Accounts;
// {
//   console.log(match.params.id);
// }
