import {
  makeStyles,
  Theme,
  createStyles,
  createMuiTheme,
  ThemeProvider,
} from "@material-ui/core";
// import Alert from '@material-ui/lab/Alert';
import ActiveProposals from "../components/dao/ActiveProposals";

import React, { Dispatch, Fragment, useEffect } from "react";
import { blueGrey } from "@material-ui/core/colors";
import PlutusDaoState from "../components/dao/PlutusDaoState";
import { useDispatch } from "react-redux";
import {
  getDaoInfo,
  getProposals,
  IDaoActions,
} from "../store/actions/daoActions";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      "& > *": {
        margin: theme.spacing(1),
        width: "25ch",
      },
    },
    toolbar: {
      display: "flex",
      alignItems: "center",
      justifyContent: "flex-end",
      padding: theme.spacing(0, 1),
      // necessary for content to be below app bar
      ...theme.mixins.toolbar,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
  })
);

let theme = createMuiTheme({
  palette: {
    primary: { main: blueGrey[600] },
  },
});
const Dao: React.FC = () => {
  const classes = useStyles();
  //   const accountDispatch = useDispatch<Dispatch<IBraidAccountActions>>();
  const daoDispatch = useDispatch<Dispatch<IDaoActions>>();
  useEffect(() => {
    daoDispatch(getProposals());
    daoDispatch(getDaoInfo());
  }, [daoDispatch]);

  return (
    <Fragment>
      <ThemeProvider theme={theme}>
        <main className={classes.content}>
          <div className={classes.toolbar} />
          <PlutusDaoState />
          <ActiveProposals />
        </main>
      </ThemeProvider>
    </Fragment>
  );
};

export default Dao;
