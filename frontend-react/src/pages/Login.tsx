import {
  createMuiTheme,
  makeStyles,
  ThemeProvider,
  Typography,
  createStyles,
  Theme,
  Button,
  Card,
  CardActions,
  CardContent,
  AppBar,
  Toolbar,
  TextField,
} from "@material-ui/core";
import React, { Dispatch, useState } from "react";
import { blueGrey } from "@material-ui/core/colors";
import { useDispatch } from "react-redux";
import { IAlert } from "../store/reducers/alertReducer";
import { IAlertActions, setAlert } from "../store/actions/alertActions";
import {
  IInitProxyActions,
  initBraidProxy,
} from "../store/actions/proxyActions";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      minWidth: 275,
      backgroundColor: "red",
    },
    cardBackground: {
      background: "linear-gradient(90deg, #e3ffe7 0%, #d9e7ff 100%)",
    },
    center: {
      marginTop: "0 auto",
      textAlign: "center",
      alignItems: "center",
      width: "100%",
    },
    middle: {
      margin: "0",
      position: "absolute",
      top: "40%",
      left: "50%",
      marginRight: "-50%",
      transform: "translate(-50%, -50%)",
      minWidth: "45rem",
    },
    backgroundMain: {
      backgroundColor: "red",
    },
    textField: {
      margin: 10,
    },
    loginButton: {
      minWidth: "10rem",
      marginTop: 30,
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(["width", "margin"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
  })
);

let theme = createMuiTheme({
  palette: {
    primary: { main: blueGrey[600] },
    background: {
      default: blueGrey[600],
    },
  },
});

type props = {
  id?: string;
};
const Login: React.FC<props> = ({ id }) => {
  const classes = useStyles();

  const alertDispatch = useDispatch<Dispatch<IAlertActions>>();
  const proxyDispatch = useDispatch<Dispatch<IInitProxyActions>>();

  const [credentials, setCredentials] = useState({
    username: "",
    password: "",
    url: "",
  });

  const handleChange = (e: any) => {
    setCredentials({ ...credentials, [e.target.name]: e.target.value });
  };

  const handleLogin = (event: any) => {
    event.preventDefault();

    if (credentials.password && credentials.username)
      return proxyDispatch(initBraidProxy(credentials));

    const alert: IAlert = {
      msg: "Username and password is required",
      alertType: "error",
    };
    return alertDispatch(setAlert(alert));

    // console.debug(
    //   "username = ",
    //   credentials.username,
    //   " password = ",
    //   "*".repeat(credentials.password.length)
    // );

    // try {
    //   if (credentials.username) {
    //     console.debug("attempting to connect with credentials");
    //     initProxy(credentials);
    //     loginDispatch(setLogin(true));
    //   } else {
    //     console.debug(
    //       "username was blank. attempting to connect without credentials"
    //     );
    //     initProxy(null);
    //     const alert: IAlert = {
    //       msg: "Username can't be empty",
    //       alertType: "error",
    //     };
    //     alertDispatch(setAlert(alert));
    //   }
    // } catch (err) {
    //   const alert: IAlert = {
    //     msg: "Can't find user",
    //     alertType: "error",
    //   };
    //   alertDispatch(setAlert(alert));
    // }
  };

  return (
    <ThemeProvider theme={theme}>
      <div className={`${classes.root}`}>
        <AppBar color="primary" position="fixed" className={classes.appBar}>
          <Toolbar>
            <Typography variant="h6" noWrap={true}>
              Cordite - Dashboard
            </Typography>
          </Toolbar>
        </AppBar>
        <Card className={`${classes.middle} ${classes.cardBackground}`}>
          <CardContent>
            <form onSubmit={handleLogin}>
              <div className={classes.center}>
                <Typography variant="h5">
                  <b style={{ color: "#546e7a" }}>LOGIN TO CORDITE</b>
                </Typography>
              </div>
              <div className={classes.center} style={{ marginTop: 30 }}>
                {/* 
              TODO: @Nisha @Mark - please re-enable this and bind value to config (using react async magic)
              <TextField
                className={classes.textField}
                color="primary"
                id="outlined-basic"
                label="URL"
                variant="outlined"
                name="url"
                onChange={handleChange}
              /> 
              <br></br>
              */}
                <TextField
                  className={classes.textField}
                  autoFocus
                  color="primary"
                  id="outlined-basic"
                  label="User Name"
                  variant="outlined"
                  name="username"
                  onChange={handleChange}
                />
                <br></br>
                <TextField
                  className={classes.textField}
                  color="primary"
                  id="outlined-basic"
                  label="Password"
                  variant="outlined"
                  name="password"
                  type="password"
                  onChange={handleChange}
                />
              </div>
              <div className={classes.center}>
                <Button
                  type="submit"
                  className={classes.loginButton}
                  style={{ height: "55px" }}
                  variant="contained"
                  color="primary"
                  onClick={handleLogin}
                >
                  Login
                </Button>
              </div>
            </form>
          </CardContent>
          <CardActions></CardActions>
        </Card>
      </div>
    </ThemeProvider>
  );
};
export default Login;
