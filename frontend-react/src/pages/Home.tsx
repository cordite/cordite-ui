import React, { Dispatch, Fragment, useEffect } from "react";
import {
  makeStyles,
  Theme,
  createStyles,
  ThemeProvider,
  Card,
  Typography,
  CardContent,
  Chip,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
  Divider,
  List,
} from "@material-ui/core";
import { theme } from "../components/shared/style";
import {
  getNodeInfo,
  INodeInfoActions,
} from "../store/actions/nodeInfoActions";
import { useDispatch, useSelector } from "react-redux";
import { AppState } from "../store/reducers";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      maxWidth: "30rem",
      width: "30rem",
    },
    media: {
      height: 140,
    },
    toolbar: {
      display: "flex",
      alignItems: "center",
      justifyContent: "flex-end",
      padding: theme.spacing(0, 1),
      // necessary for content to be below app bar
      ...theme.mixins.toolbar,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
    center: {
      display: "flex",
      width: "100%",
      alignContent: "center",
      justifyContent: "center",
      textAlign: "center",
    },
    padding20: {
      padding: 20,
    },
    alignLeft: {
      textAlign: "left",
    },
    alignRight: {
      textAlign: "right",
    },
    listItem: {
      padding: 10,
    },
    cardHeader: {
      padding: 10,
      backgroundColor: "#78909c",
      color: "#fff",
    },
  })
);
const Home: React.FC = () => {
  const classes = useStyles();
  const nodeInfoDispatch = useDispatch<Dispatch<INodeInfoActions>>();
  const { organization, organizationUnit, city, country } = useSelector(
    (state: AppState) => state.nodeInfo
  );

  useEffect(() => {
    nodeInfoDispatch(getNodeInfo());
  }, [nodeInfoDispatch]);
  return (
    <Fragment>
      <ThemeProvider theme={theme}>
        <main className={classes.content}>
          <div className={classes.toolbar} />
          <div className={classes.center}>
            <Card>
              <div className={classes.cardHeader}>
                <Typography gutterBottom variant="h5" component="h2">
                  Node Info
                </Typography>
              </div>
              <CardContent>
                <List dense className={classes.root}>
                  <ListItem className={classes.listItem}>
                    <ListItemText primary={`Organization (O)`} />
                    <ListItemSecondaryAction>
                      <Chip label={organization} color="primary" />
                    </ListItemSecondaryAction>
                  </ListItem>
                  <Divider />
                  <ListItem className={classes.listItem}>
                    <ListItemText primary={`City (L)`} />
                    <ListItemSecondaryAction>
                      <Chip label={city} color="primary" />
                    </ListItemSecondaryAction>
                  </ListItem>
                  <Divider />
                  <ListItem className={classes.listItem}>
                    <ListItemText primary={`Country (C)`} />
                    <ListItemSecondaryAction>
                      <Chip label={country} color="primary" />
                    </ListItemSecondaryAction>
                  </ListItem>
                  <Divider />
                  <ListItem className={classes.listItem}>
                    <ListItemText primary={`Organization Unit (OU)`} />
                    <ListItemSecondaryAction>
                      <Chip label={organizationUnit} color="primary" />
                    </ListItemSecondaryAction>
                  </ListItem>
                  <Divider />
                </List>
              </CardContent>
            </Card>
          </div>
        </main>
      </ThemeProvider>
    </Fragment>
  );
};
export default Home;
