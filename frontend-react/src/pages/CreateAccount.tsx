import {
  makeStyles,
  Theme,
  createStyles,
  ThemeProvider,
  Button,
  CircularProgress,
} from "@material-ui/core";
// import Alert from '@material-ui/lab/Alert';

import React, { Dispatch, Fragment, useState } from "react";
import TextField from "@material-ui/core/TextField";
import { useDispatch } from "react-redux";
import {
  createAccount,
  IBraidAccountActions,
} from "../store/actions/braidActions";
import { theme } from "../components/shared/style";
import { ApplicationConfig } from "../ApplicationConfig";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      "& > *": {
        margin: theme.spacing(1),
        width: "25ch",
      },
    },
    toolbar: {
      display: "flex",
      alignItems: "center",
      justifyContent: "flex-end",
      padding: theme.spacing(0, 1),
      // necessary for content to be below app bar
      ...theme.mixins.toolbar,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
    alert: {
      position: "fixed",
      bottom: 20,
      width: "80%",
    },
  })
);

const CreateAccount: React.FC = () => {
  const classes = useStyles();
  const accountDispatch = useDispatch<Dispatch<IBraidAccountActions>>();

  const [account, setAccountId] = useState({
    accountId: "",
    notary: ApplicationConfig.singleton.notaryName,
  });
  const { accountId, notary } = account;

  const loading = false;

  const handleAccountIdChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ): void => {
    e.preventDefault();
    setAccountId({ ...account, [e.target.name]: e.target.value });
  };

  const handleCreateAccount = () => {
    setAccountId({ accountId: "", notary: "" });
    // console.log(accountId, "  ", notary);
    accountDispatch(createAccount({ accountId: accountId, notary: notary }));
  };

  return (
    <Fragment>
      <ThemeProvider theme={theme}>
        <main className={classes.content}>
          <div className={classes.toolbar} />

          <form className={classes.root} noValidate autoComplete="off">
            <TextField
              color="primary"
              id="outlined-basic"
              label="Enter Account Name"
              variant="outlined"
              name="accountId"
              value={accountId}
              onChange={handleAccountIdChange}
            />
            <Button
              style={{ height: "55px" }}
              variant="contained"
              color="primary"
              disabled={loading}
              onClick={handleCreateAccount}
            >
              Add Account &nbsp;
              {loading && <CircularProgress size={24} color="primary" />}
            </Button>
          </form>
        </main>
      </ThemeProvider>
    </Fragment>
  );
};

export default CreateAccount;
