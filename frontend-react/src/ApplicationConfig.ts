export class ApplicationConfig {
  constructor(
    public apiUrl: string,
    public notaryName: string,
    public cslName: string
  ) {}

  static async loadConfig() {
    if (!ApplicationConfig.singleton) {
      try {
        ApplicationConfig.singleton = await ApplicationConfig.getApplicationConfigFromServer();
        console.debug("retrieved config.json from the server");
      } catch (err) {
        console.error(
          "failed to load config from server. attempting to use the build time environment settings",
          err
        );
        ApplicationConfig.singleton = ApplicationConfig.getApplicationConfigFromEnvironmentVars();
      }
      console.debug(
        `config set to: ${JSON.stringify(ApplicationConfig.singleton)}`
      );
    }
  }

  static singleton: ApplicationConfig;

  private static getApplicationConfigFromEnvironmentVars(): ApplicationConfig {
    return new ApplicationConfig(
      ApplicationConfig.getEnvVar(
        "CORDITE_UI_NODE_API",
        "https://localhost:9083/api/"
      ),
      ApplicationConfig.getEnvVar(
        "CORDITE_UI_DEFAULT_NOTARY_NAME",
        "OU=Cordite Foundation, O=Notary, L=London, C=GB"
      ),
      ApplicationConfig.getEnvVar(
        "CORDITE_UI_CSL_NAME",
        "CN=csl.cordite.foundation, O=Cordite Society Limited, L=London, C=GB"
      )
    );
  }

  private static async getJSON<T>(url: string): Promise<T> {
    const res = await fetch(url);
    const json = res.json() as unknown;
    return json as T;
  }

  private static async getApplicationConfigFromServer(): Promise<ApplicationConfig> {
    return await ApplicationConfig.getJSON<ApplicationConfig>("/config.json");
  }

  private static getEnvVar(varName: string, defaultValue: string): string {
    return process.env[`REACT_APP_${varName}`] || defaultValue;
  }
}
