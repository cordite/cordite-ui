import * as types from "../types";
import { ActionCreator, Action } from "redux";

//Alert Actions
export interface ILoginActions extends Action {
  type: types.SET_LOGIN | types.REMOVE_BRAID_PROXY;
  payload?: boolean;
}

//Alert Action : Setting alert, caught in saga
const setLogin: ActionCreator<ILoginActions> = (payload: boolean) => ({
  type: types.SET_LOGIN,
  payload: payload,
});
const logoutUser: ActionCreator<ILoginActions> = () => ({
  type: types.REMOVE_BRAID_PROXY,
});

export { setLogin, logoutUser };
