import * as types from "../types";
import { ActionCreator, Action } from "redux";

interface IProxyCredentials {
  username: string;
  password: string;
  url: string;
}

//Node Info Actions
export interface IInitProxyActions extends Action {
  type:
    | types.INIT_BRAID_PROXY
    | types.UPDATE_BRAID_PROXY
    | types.REMOVE_BRAID_PROXY
    | types.GET_PROPOSAL_UPDATE;
  payload?: IProxyCredentials | any;
}

const initBraidProxy: ActionCreator<IInitProxyActions> = (
  payload: IProxyCredentials
) => ({
  type: types.INIT_BRAID_PROXY,
  payload: payload,
});
const removeBraidProxy: ActionCreator<IInitProxyActions> = () => ({
  type: types.REMOVE_BRAID_PROXY,
});

const updateBraidProxy: ActionCreator<IInitProxyActions> = (payload: any) => ({
  type: types.UPDATE_BRAID_PROXY,
  payload: payload,
});

const getProposalUpdate: ActionCreator<IInitProxyActions> = () => ({
  type: types.GET_PROPOSAL_UPDATE,
});

export {
  initBraidProxy,
  updateBraidProxy,
  removeBraidProxy,
  getProposalUpdate,
};
