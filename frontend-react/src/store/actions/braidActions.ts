import * as types from "../types";
import { ActionCreator, Action } from "redux";
import { IBalanceState } from "../reducers/braidReducer";
import { ITokenState, ITransferToken } from "../reducers/tokenReducer";
//Braid Actions
export interface IBraidAccountActions extends Action {
  type:
    | types.BRAID_LIST_ACCOUNTS
    | types.BRAID_CREATE_ACCOUNT
    | types.BRAID_UPDATE_ACCOUNTS_SAGA
    | types.BRAID_UPDATE_ACCOUNT;
  payload?: { accountId: string; notary: string } | string[];
}
export interface IBraidAccountBalanceActions extends Action {
  type:
    | types.BRAID_GET_ACCOUNT_BALANCE
    | types.BRAID_UPDATE_ACCOUNT_BALANCE_SAGA;
}
export interface IBraidTokenActions extends Action {
  type:
    | types.BRAID_GET_TOKEN_LIST
    | types.BRAID_UPDATE_TOKEN_LIST
    | types.BRAID_TRANSFER_TOKEN;
  payload?: string | ITokenState | ITransferToken;
}

//Action Creators

//Token Action : Calling listTokenList() from saga
const getTokenList: ActionCreator<IBraidTokenActions> = (payload: string) => ({
  type: types.BRAID_GET_TOKEN_LIST,
  payload: payload,
});

const updateTokenList: ActionCreator<IBraidTokenActions> = (
  payload: ITokenState
) => ({
  type: types.BRAID_UPDATE_TOKEN_LIST,
  payload: payload,
});

const transferToken: ActionCreator<IBraidTokenActions> = (
  payload: ITransferToken
) => ({
  type: types.BRAID_TRANSFER_TOKEN,
  payload: payload,
});

//call list account from braid api .listaccount() catch in redux-saga
const listAccounts: ActionCreator<IBraidAccountActions> = () => ({
  type: types.BRAID_LIST_ACCOUNTS,
});

const updateAccount: ActionCreator<IBraidAccountActions> = () => ({
  type: types.BRAID_UPDATE_ACCOUNT,
});

//take data from braid api .listaccount() as payload called in redux-saga
const updateAccountList: ActionCreator<IBraidAccountActions> = (
  payload: string[]
) => ({
  type: types.BRAID_UPDATE_ACCOUNTS_SAGA,
  payload: payload,
});

//take id and notary as input catch in redux-saga
const createAccount: ActionCreator<IBraidAccountActions> = (payload: {
  accountId: string;
  notary: string;
}) => ({
  type: types.BRAID_CREATE_ACCOUNT,
  payload: payload,
});

const getAccountBalance: ActionCreator<IBraidAccountBalanceActions> = (
  payload: string
) => ({
  type: types.BRAID_GET_ACCOUNT_BALANCE,
  payload: payload,
});
const updateAccountBalance: ActionCreator<IBraidAccountBalanceActions> = (
  payload: IBalanceState
) => ({
  type: types.BRAID_UPDATE_ACCOUNT_BALANCE_SAGA,
  payload: payload,
});

export {
  listAccounts,
  updateAccountList,
  createAccount,
  updateAccountBalance,
  updateAccount,
  getAccountBalance,
  getTokenList,
  updateTokenList,
  transferToken,
};
