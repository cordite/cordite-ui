import * as types from '../types';
import { ActionCreator, Action } from 'redux';
import { ITransactionPayload, ITransactionsState } from '../reducers/transactionsReducer';

//Alert Actions
export interface ITransactionsActions extends Action {
  type:
     types.BRAID_GET_TRANSACTIONS
    | types.BRAID_UPDATE_TRANSACTIONS,
  payload: ITransactionsState
}
export interface ITransactionsGetActions extends Action {
  type:
     types.BRAID_GET_TRANSACTIONS
  payload: ITransactionPayload
}

//Alert Action : Setting alert, caught in saga
const getTransactions: ActionCreator<ITransactionsGetActions> = (payload: ITransactionPayload) => ({
  type: types.BRAID_GET_TRANSACTIONS,
  payload: payload
});

const updateTransactions: ActionCreator<ITransactionsActions> = (payload: ITransactionsState) => ({
  type: types.BRAID_UPDATE_TRANSACTIONS,
  payload: payload
});



export { getTransactions, updateTransactions };

// 1. The Date
// 2. The Token (just the symbol)
// 3. The Amount
// 4. The Destination Account name 
// 5. The description - you probably want to limit this to a fixed number of characters

//ledger.transactionsForAccount('ac1',{'pageNumber':1,pageSize:20})
// [
//   {
//       "participants": [{ "name": "OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US", "owningKey": "GfHq2tTVk9z4eXgyWEYRQUdLJVdKd7T6EAktoP4U3EYTYUKL3DL2VoEjV8UC" }],
//       "command": "Move",
//       "amounts": [
//           {
//               "accountAddress": { "accountId": "ac1", "party": "OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US", "uri": "ac1@OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US" },
//               "amount": { "quantity": "-20.00", "amountType": { "symbol": "R3C", "issuerName": "OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US", "uri": "R3C:OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US" } }
//           },
//           {
//               "accountAddress": { "accountId": "ac2", "party": "OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US", "uri": "ac2@OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US" },
//               "amount": { "quantity": "20.00", "amountType": { "symbol": "R3C", "issuerName": "OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US", "uri": "R3C:OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US" } }
//           }
//       ],
//       "description": "temp",
//       "transactionTime": "2020-10-09T12:04:50.975Z",
//       "transactionId": "3q2pKDfjfmxu8KZRySmfVqNYXXmo4fddizAXFdrYNRzo"
//   },
//   {
//       "participants": [{ "name": "OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US", "owningKey": "GfHq2tTVk9z4eXgyWEYRQUdLJVdKd7T6EAktoP4U3EYTYUKL3DL2VoEjV8UC" }],
//       "command": "Issue",
//       "amounts": [
//           {
//               "accountAddress": { "accountId": "ac1", "party": "OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US", "uri": "ac1@OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US" },
//               "amount": { "quantity": "50.00", "amountType": { "symbol": "R3C", "issuerName": "OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US", "uri": "R3C:OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US" } }
//           }
//       ],
//       "description": "you got money",
//       "transactionTime": "2020-10-09T11:59:57.776Z",
//       "transactionId": "44H24YvKe8ZPnkHMkUsoDEFY1dvHHKDgiPJRmz4g2qJ7"
//   }
// ]
