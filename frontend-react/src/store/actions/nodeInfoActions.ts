import * as types from "../types";
import { ActionCreator, Action } from "redux";
import { INodeInfoState } from "../reducers/nodeInfoReducer";

//Node Info Actions
export interface INodeInfoActions extends Action {
  type: types.GET_NODE_INFO | types.SET_NODE_INFO;
  payload?: INodeInfoState;
}

const getNodeInfo: ActionCreator<INodeInfoActions> = () => ({
  type: types.GET_NODE_INFO,
});

const setNodeInfo: ActionCreator<INodeInfoActions> = (
  payload: INodeInfoState
) => ({
  type: types.SET_NODE_INFO,
  payload: payload,
});

export { getNodeInfo, setNodeInfo };
