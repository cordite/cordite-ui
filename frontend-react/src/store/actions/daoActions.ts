import * as types from "../types";
import { ActionCreator, Action } from "redux";
import { IDaoState, IPmdState } from "../reducers/daoReducer";

//Alert Actions
export interface IDaoActions extends Action {
  type:
    | types.GET_PROPOSALS
    | types.SET_PROPOSALS
    | types.SET_DAO_INFO
    | types.GET_DAO_INFO
    | types.VOTE_DOWN
    | types.VOTE_UP;
  payload?: IDaoState | IPmdState;
}

//Alert Action : Setting alert, caught in saga
const getProposals: ActionCreator<IDaoActions> = () => ({
  type: types.GET_PROPOSALS,
});
const getDaoInfo: ActionCreator<IDaoActions> = () => ({
  type: types.GET_DAO_INFO,
});
const setDaoInfo: ActionCreator<IDaoActions> = (payload: IPmdState) => ({
  type: types.SET_DAO_INFO,
  payload: payload,
});
const setProposals: ActionCreator<IDaoActions> = (payload: IDaoState) => ({
  type: types.SET_PROPOSALS,
  payload: payload,
});
const voteUpProposal: ActionCreator<IDaoActions> = () => ({
  type: types.VOTE_UP,
});
const voteDownProposal: ActionCreator<IDaoActions> = () => ({
  type: types.VOTE_DOWN,
});
export {
  getProposals,
  setProposals,
  voteDownProposal,
  voteUpProposal,
  getDaoInfo,
  setDaoInfo,
};
