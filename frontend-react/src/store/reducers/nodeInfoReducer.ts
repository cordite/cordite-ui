import * as types from "../types";

export interface INodeInfoState {
  organization: string;
  city: string;
  country: string;
  organizationUnit: string;
  fullName?: string;
}

export const initialNodeInfoState: INodeInfoState = {
  organization: "",
  city: "",
  country: "",
  organizationUnit: "",
  fullName: "",
};

export const nodeInfoReducer = (
  state: INodeInfoState = initialNodeInfoState,
  action: {
    type: types.SET_NODE_INFO | types.GET_NODE_INFO;
    payload: INodeInfoState;
  }
): INodeInfoState => {
  const { type, payload } = action;

  switch (type) {
    case types.SET_NODE_INFO:
      return payload;
    default:
      return state;
  }
};
