import * as types from '../types';

export interface IBalances {
  quantity: string;
  amountType: {
    symbol: string;
    issuerName: string;
  };
}

export interface IBalanceState {
  accountId: string;
  balances: IBalances[];
}

export const initialBalanceState: IBalanceState = {
  accountId: '',
  balances: [
    {
      quantity: '',
      amountType: {
        symbol: '',
        issuerName: '',
      },
    },
    
  ],
};

export const balanceReducer = (
  state: IBalanceState = initialBalanceState,
  action: {
    type:
      | types.BRAID_UPDATE_ACCOUNT_BALANCE_SAGA
      | types.BRAID_GET_ACCOUNT_BALANCE;
    payload?: IBalanceState;
  }
): IBalanceState => {
  const { type, payload } = action;

  switch (type) {
    case types.BRAID_UPDATE_ACCOUNT_BALANCE_SAGA:
      if (payload) {
        return {
          ...state,
          accountId: payload.accountId,
          balances: payload.balances,
        };
      }
      return state;
    default:
      return state;
  }
};
