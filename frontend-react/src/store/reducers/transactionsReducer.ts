import { ITransactionsActions } from "../actions/transactionsActions";
import * as types from "../types";

export interface ITransactionsState {
  transactions: ITransaction[];
}

// export interface ITransaction {
//   date?: string; //transaction.transactionTime
//   token?: string; //transaction.amounts[0].amount.amountType.symbol
//   amount?: number; //transaction.amounts[1].amount.quantity
//   toAcc?: string; //transaction.amounts[1].accountAddress.uri;
//   type?: "Move" | "Issue" | null; //transaction.command
//   fromAcc?: string; //transaction.amounts[0].accountAddress.uri;
//   description?: string; //transaction.description
// }

export interface ITransaction {
  date?: string; //transaction.transactionTime
  type?: "Move" | "Issue" | null; //transaction.command
  description?: string; //transaction.description
  token_0?: string; //transaction.amounts[0].amount.amountType.symbol
  token_1?: string; //transaction.amounts[1].amount.amountType.symbol
  acc_0_id?: string; //transaction.amounts[0].accountAddress.accountId
  acc_1_id?: string; //transaction.amounts[1].accountAddress.accountId;
  acc_0_uri?: string; //transaction.amounts[0].accountAddress.uri;
  acc_1_uri?: string; //transaction.amounts[1].accountAddress.uri;
  amount_0?: string; //transaction.amounts[0].amount.quantity
  amount_1?: string; //transaction.amounts[1].amount.quantity
}

export interface ITransactionPayload {
  accountId?: string;
  page: {
    pageNumber: number;
    pageSize: number;
  };
}
const initialTransactionsState: ITransactionsState = {
  transactions: [
    {
      date: "",
      type: null,
      description: "",
      token_0: "",
      token_1: "",
      amount_0: "",
      amount_1: "",
      acc_0_id: "",
      acc_1_id: "",
      acc_0_uri: "",
      acc_1_uri: "",
    },
  ],
};

export const transactionsReducer = (
  state: ITransactionsState = initialTransactionsState,
  action: ITransactionsActions
): ITransactionsState => {
  const { type, payload } = action;
  switch (type) {
    case types.BRAID_UPDATE_TRANSACTIONS:
      // console.log("payload in reducer = ", JSON.stringify(payload));
      if (typeof payload === typeof state && payload) {
        return { ...state, transactions: payload.transactions };
      }
      return state;
    default:
      return state;
  }
};
