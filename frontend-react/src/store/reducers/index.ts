import { AnyAction, combineReducers } from "redux";
import { accountsReducer } from "./accountsReducer";
import { balanceReducer } from "./balanceReducer";
import { tokenReducer } from "./tokenReducer";
import { alertReducer } from "./alertReducer";
import { transactionsReducer } from "./transactionsReducer";
import { loginReducer } from "./loginReducer";
import { daoReducer } from "./daoReducer";
import { nodeInfoReducer } from "./nodeInfoReducer";
import { proxyReducer } from "./proxyReducer";
const _ = (state: any = 0, _: AnyAction) => state;
const rootReducer = combineReducers({
  _,
  accountList: accountsReducer,
  accountBalance: balanceReducer,
  tokenList: tokenReducer,
  alertList: alertReducer,
  transactionList: transactionsReducer,
  loginState: loginReducer,
  daoState: daoReducer,
  nodeInfo: nodeInfoReducer,
  braidProxy: proxyReducer,
});
export type AppState = ReturnType<typeof rootReducer>;
export default rootReducer;
