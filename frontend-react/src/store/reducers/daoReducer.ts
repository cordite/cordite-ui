import * as types from "../types";

export interface IPlutusDaoState {
  votes?: IVote[];
  name?: string;
  pmd: IPmdState;
  mmd?: {
    minimumMemberCount: number;
    hasMinNumberOfMembers: boolean;
  };
  members?: { name: string; owningKey: string }[];
}

export interface IPmdState {
  plutusDaoOwner: string;
  currentMintingRate: number;
  totalIssued: number;
  minNextProposal: string;
  proposalPeriod: string;
  accountName: string;
  issuableTokens: {
    symbol: string;
    exponent: number;
    issuerName: string;
  }[];
}

export interface IVote {
  voter: {
    name: string;
    owningKey: string;
  };
  voteType: {
    type: "DOWN" | "UP" | null;
  };
}

export interface IDaoState {
  plutusDaoState?: IPlutusDaoState;
  proposer: string;
  tokenType: string;
  basisPointChange: string;
  lifeCycleState: string;
  voteResult: string;
  currentUserVoted?: string;
}

export const initialDaoState: IDaoState = {
  plutusDaoState: {
    votes: [
      {
        voteType: { type: null },
        voter: {
          name: "",
          owningKey: "",
        },
      },
    ],
    name: "",
    pmd: {
      plutusDaoOwner: "",
      currentMintingRate: 0,
      totalIssued: 0,
      minNextProposal: "",
      proposalPeriod: "",
      accountName: "",
      issuableTokens: [
        {
          symbol: "",
          exponent: 2,
          issuerName: "",
        },
      ],
    },
    mmd: {
      minimumMemberCount: 1,
      hasMinNumberOfMembers: true,
    },
    members: [
      {
        name: "",
        owningKey: "",
      },
    ],
  },
  proposer: "",
  tokenType: "",
  basisPointChange: "",
  lifeCycleState: "",
  voteResult: "",
  currentUserVoted: "",
};

export const daoReducer = (
  state: IDaoState = initialDaoState,
  action: {
    type:
      | types.SET_PROPOSALS
      | types.GET_PROPOSALS
      | types.GET_DAO_INFO
      | types.SET_DAO_INFO;
    payload: IDaoState | IPmdState;
  }
): IDaoState => {
  const { type, payload } = action;
  let pmdPayload = payload as IPmdState;
  let daoStatePayload = payload as IDaoState;

  switch (type) {
    case types.SET_PROPOSALS:
      // console.log('payload in reducer = ', JSON.stringify(payload));
      return daoStatePayload;

    case types.SET_DAO_INFO:
      return {
        ...state,
        plutusDaoState: { ...state.plutusDaoState, pmd: pmdPayload },
      };

    default:
      return state;
  }
};
