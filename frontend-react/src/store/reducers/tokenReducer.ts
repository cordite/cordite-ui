import * as types from '../types';

export interface IToken {
  tokenType: string;
  uri: string;
}

export interface ITokenState {
  tokens: IToken[];
}

export interface ITransferToken {
  amt: number;
  uri: string;
  fromAcc: string;
  toAcc: string;
  description: string;
  notary: string;
}

export const initialTokenState: ITokenState = {
  tokens: [
    {
      tokenType: '',
      uri: '',
    },
  ],
};

export const tokenReducer = (
  state: ITokenState = initialTokenState,
  action: {
    type:
      | types.BRAID_GET_TOKEN_LIST
      | types.BRAID_UPDATE_TOKEN_LIST
      | types.BRAID_TRANSFER_TOKEN;
    payload?: ITokenState;
  }
): ITokenState => {
  const { type, payload } = action;

  switch (type) {
    case types.BRAID_UPDATE_TOKEN_LIST:
      if (payload) {
        return payload;
      }
      return state;
    default:
      return state;
  }
};

//ledger.listTokenTypes()
/*
[
  {
   symbol: 'R2C',
   exponent: 2,
   description: '',
   issuer: {
    name: 'OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US',
    owningKey: 'GfHq2tTVk9z4eXgyHTBh1XWofFvJwzAkT35DLwKtNo6By9zWzhSMGCvqmbc6'
   },
   linearId: { externalId: 'R2C', id: '02c7783c-f597-4f59-b3ec-fd35ed9ffc20' },
   metaData: null,
   settlements: [],
   participants: [ [Object] ],
   descriptor: {
    symbol: 'R2C',
    issuerName: 'OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US',
    uri: 'R2C:OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US'
   }
  },
  {
   symbol: 'R3C',
   exponent: 2,
   description: '',
   issuer: {
    name: 'OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US',
    owningKey: 'GfHq2tTVk9z4eXgyHTBh1XWofFvJwzAkT35DLwKtNo6By9zWzhSMGCvqmbc6'
   },
   linearId: { externalId: 'R3C', id: '5d1ef05e-e45b-4c6d-9eb8-d3db2c0644e0' },
   metaData: null,
   settlements: [],
   participants: [ [Object] ],
   descriptor: {
    symbol: 'R3C',
    issuerName: 'OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US',
    uri: 'R3C:OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US'
   }
  }
 ]
 */
