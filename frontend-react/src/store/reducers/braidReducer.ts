import * as types from '../types';

export interface IAccountState {
  accounts: string[];
  loading: boolean;
}
export interface IBalances {
  quantity: string;
  amountType: {
    symbol: string;
    issuerName: string;
  };
}

export interface ITransactions{

}

export interface IBalanceState {
  accountId: string;
  balances: IBalances[];
}

export const initialAccountState: IAccountState = {
  accounts: [''],
  loading: false,
};

export const initialBalanceState: IBalanceState = {
  accountId: '',
  balances: [
    {
      quantity: '',
      amountType: {
        symbol: '',
        issuerName: '',
      },
    },
  ],
};

export const AccountsReducer = (
  state: IAccountState = initialAccountState,
  action: {
    type:
      | types.BRAID_UPDATE_ACCOUNTS_SAGA
      | types.BRAID_LIST_ACCOUNTS
      | types.BRAID_CREATE_ACCOUNT
      | types.BRAID_UPDATE_ACCOUNT;
    payload?: [string];
  }
): IAccountState => {
  const { type, payload } = action;

  switch (type) {
    case types.BRAID_UPDATE_ACCOUNTS_SAGA:
      if (payload) return { ...state, accounts: payload, loading: false };
      else return state;
    case types.BRAID_CREATE_ACCOUNT:
      return { ...state, loading: true };
    default:
      return state;
  }
};

export const balanceReducer = (
  state: IBalanceState = initialBalanceState,
  action: {
    type:
      | types.BRAID_UPDATE_ACCOUNT_BALANCE_SAGA
      | types.BRAID_GET_ACCOUNT_BALANCE;
    payload?: IBalanceState;
  }
): IBalanceState => {
  const { type, payload } = action;

  switch (type) {
    case types.BRAID_UPDATE_ACCOUNT_BALANCE_SAGA:
      if (payload) {
        return {
          ...state,
          accountId: payload.accountId,
          balances: payload.balances,
        };
      }
      return state;
    default:
      return state;
  }
};

