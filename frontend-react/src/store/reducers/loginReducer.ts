import * as types from "../types";

export interface ILoginState {
  login: boolean;
}

export const initialLoginState: ILoginState = {
  login: false,
};

export const loginReducer = (
  state: ILoginState = initialLoginState,
  action: {
    type: types.SET_LOGIN | types.REMOVE_BRAID_PROXY;
    payload: boolean;
  }
): ILoginState => {
  const { type, payload } = action;

  switch (type) {
    case types.SET_LOGIN:
      return { login: payload };
    default:
      return state;
  }
};
