import * as types from "../types";

export interface IProxyState {
  braidProxy: any | undefined;
}

export const initialProxyState: IProxyState = { braidProxy: undefined };

export const proxyReducer = (
  state: IProxyState = initialProxyState,
  action: {
    type:
      | types.INIT_BRAID_PROXY
      | types.UPDATE_BRAID_PROXY
      | types.REMOVE_BRAID_PROXY
      | types.GET_PROPOSAL_UPDATE;
    payload: any;
  }
): IProxyState => {
  const { type, payload } = action;

  switch (type) {
    case types.UPDATE_BRAID_PROXY:
      return { braidProxy: payload };

    default:
      return state;
  }
};
