import * as types from '../types';

export interface IAccountState {
  accounts: string[];
  loading: boolean;
}
export const initialAccountState: IAccountState = {
  accounts: [],
  loading: false,
};

export const accountsReducer = (
  state: IAccountState = initialAccountState,
  action: {
    type:
      | types.BRAID_UPDATE_ACCOUNTS_SAGA
      | types.BRAID_LIST_ACCOUNTS
      | types.BRAID_CREATE_ACCOUNT
      | types.BRAID_UPDATE_ACCOUNT;
    payload?: [string];
  }
): IAccountState => {
  const { type, payload } = action;

  switch (type) {
    case types.BRAID_UPDATE_ACCOUNTS_SAGA:
      if (payload) return { ...state, accounts: payload, loading: false };
      else return state;
    case types.BRAID_CREATE_ACCOUNT:
      return { ...state, loading: true };
    default:
      return state;
  }
};
