import { call, put } from "redux-saga/effects";
import { setAlert } from "../actions/alertActions";
import { getProposals, setDaoInfo, setProposals } from "../actions/daoActions";
import { braidProxy } from "./proxySaga";
import { IAlert } from "../reducers/alertReducer";
import { IDaoState, IPmdState, IVote } from "../reducers/daoReducer";

const daoName = "plutus";
const csl =
  "CN=csl.cordite.foundation, O=Cordite Society Limited, L=London, C=GB";

function* getProposalsSaga() {
  try {
    const daoStates = yield call(braidProxy.dao.daoInfo, daoName);
    if (daoStates.length === 0) {
      const proposal = yield call(
        braidProxy.dao.createNewMemberProposal,
        daoName,
        csl
      );
      yield call(
        braidProxy.dao.sponsorAcceptProposal,
        proposal.proposal.proposalKey,
        proposal.daoKey,
        csl
      );
      yield put(getProposals());
    } else {
      const daoState = daoStates[0];

      if (daoState.daoKey) {
        const pmd =
          daoState.modelDataMap["io.cordite.dao.plutus.PlutusModelData"];
        const mmd =
          daoState.modelDataMap[
            "io.cordite.dao.membership.MembershipModelData"
          ];
        const issuableTokens = pmd.issuableTokens;
        const members = daoState.members;
        const daoKey = daoState.daoKey;
        const proposals = yield call(braidProxy.dao.plutusProposalsFor, daoKey);
        const proposal = proposals[0];
        const votes = proposal.votes;
        const votesPayload: IVote[] = [];
        votes.forEach((vote: any) => {
          const voteSingleton: IVote = {
            voter: {
              name: vote.voter.name,
              owningKey: vote.voter.owningKey,
            },
            voteType: {
              type: vote.voteType.type,
            },
          };
          votesPayload.push(voteSingleton);
        });

        let payload: IDaoState = {
          plutusDaoState: {
            votes: votesPayload,
            name: "plutus",
            pmd: {
              plutusDaoOwner: pmd.plutusDaoOwner,
              currentMintingRate: pmd.currentMintingRate,
              totalIssued: pmd.totalIssued,
              minNextProposal: pmd.minNextProposal,
              proposalPeriod: pmd.proposalPeriod,
              accountName: pmd.accountName,
              issuableTokens: issuableTokens,
            },
            mmd: {
              minimumMemberCount: mmd.minimumMemberCount,
              hasMinNumberOfMembers: mmd.hasMinNumberOfMembers,
            },
            members: members,
          },
          proposer: proposal.proposer.name,
          tokenType: proposal.proposal.tokenType.symbol,
          basisPointChange: proposal.proposal.basisPointChange,
          lifeCycleState: proposal.lifecycleState,
          voteResult: proposal.voteResult.result,
        };
        // console.log("payload = ", JSON.stringify(payload));
        yield put(setProposals(payload));

        // const alert: IAlert = {
        //   msg: "got proposals successfully",
        //   alertType: "success",
        // };
        // yield put(setAlert(alert));
      }
    }
  } catch (error) {
    const alert: IAlert = {
      msg: error.message,
      alertType: "error",
    };
    yield put(setAlert(alert));
  }
}

function* getDaoInfoSaga() {
  try {
    const daoStates = yield call(braidProxy.dao.daoInfo, daoName);
    if (daoStates.length === 0) {
      const proposal = yield call(
        braidProxy.dao.createNewMemberProposal,
        daoName,
        csl
      );
      yield call(
        braidProxy.dao.sponsorAcceptProposal,
        proposal.proposal.proposalKey,
        proposal.daoKey,
        csl
      );
      yield put(getProposals());
    } else {
      const daoState = daoStates[0];

      if (daoState.daoKey) {
        const pmd =
          daoState.modelDataMap["io.cordite.dao.plutus.PlutusModelData"];
        const issuableTokens = pmd.issuableTokens;

        let payload: IPmdState = {
          plutusDaoOwner: pmd.plutusDaoOwner,
          currentMintingRate: pmd.currentMintingRate,
          totalIssued: pmd.totalIssued,
          minNextProposal: pmd.minNextProposal,
          proposalPeriod: pmd.proposalPeriod,
          accountName: pmd.accountName,
          issuableTokens: issuableTokens,
        };
        // console.log("payload = ", JSON.stringify(payload));
        yield put(setDaoInfo(payload));

        // const alert: IAlert = {
        //   msg: "got proposals successfully",
        //   alertType: "success",
        // };
        // yield put(setAlert(alert));
      }
    }
  } catch (error) {
    const alert: IAlert = {
      msg: error.message,
      alertType: "error",
    };
    yield put(setAlert(alert));
  }
}

function* voteUpProposalSaga() {
  try {
    const daoStates = yield call(braidProxy.dao.daoInfo, daoName);
    const daoState = daoStates[0];
    // console.log(JSON.stringify(daoState));
    const daoKey = daoState.daoKey;
    const proposals = yield call(braidProxy.dao.plutusProposalsFor, daoKey);
    const proposal = proposals[0];
    const proposalKey = proposal.proposal.proposalKey;

    yield call(braidProxy.dao.voteForProposal, proposalKey, { type: "UP" });
    yield put(getProposals());

    const alert: IAlert = {
      msg: "Vote UP successful!",
      alertType: "success",
    };
    yield put(setAlert(alert));
  } catch (error) {
    const alert: IAlert = {
      msg: error.message,
      alertType: "error",
    };
    yield put(setAlert(alert));
  }
}
function* voteDownProposalSaga() {
  try {
    const daoStates = yield call(braidProxy.dao.daoInfo, daoName);
    const daoState = daoStates[0];
    const daoKey = daoState.daoKey;
    const proposals = yield call(braidProxy.dao.plutusProposalsFor, daoKey);

    const proposal = proposals[0];
    const proposalKey = proposal.proposal.proposalKey;

    yield call(braidProxy.dao.voteForProposal, proposalKey, { type: "DOWN" });

    const alert: IAlert = {
      msg: "Vote DOWN successful!",
      alertType: "success",
    };
    yield put(setAlert(alert));
  } catch (error) {
    const alert: IAlert = {
      msg: error.message,
      alertType: "error",
    };
    yield put(setAlert(alert));
  }
}

export {
  getProposalsSaga,
  voteDownProposalSaga,
  voteUpProposalSaga,
  getDaoInfoSaga,
};

//----------------------Proposals-------------------------------------

// proposal.proposal.tokenType.symbol;
// proposal.proposal.basisPointChange;
// proposal.lifecycleState;
// proposal.voteResult;
// proposal.proposer.name;

//  [
//    {
//      proposal: {
//        "@class": "io.cordite.dao.plutus.PlutusProposal",
//        tokenType: {
//          symbol: "XCD-1603041146",-------------------
//          issuerName:
//            "CN=csl.cordite.foundation, O=Cordite Society Limited, L=London, C=GB",
//          uri:
//            "XCD-1603041146:CN=csl.cordite.foundation, O=Cordite Society Limited, L=London, C=GB",
//        },
//        basisPointChange: 0.02,-----------------------
//        proposalKey: {
//          name: "Issuance:XCD-1603041146:0.02",
//          uuid: "71a6f956-0696-47d3-b9c0-0dd234830436",
//          uniqueIdentifier: {
//            externalId: "Issuance:XCD-1603041146:0.02",
//            id: "71a6f956-0696-47d3-b9c0-0dd234830436",
//          },
//        },
//      },
//      proposer: {
//        name:---------------------------------
//          "CN=csl.cordite.foundation, O=Cordite Society Limited, L=London, C=GB",
//        owningKey:
//          "GfHq2tTVk9z4eXgyKhLHo2VzfnXBh86irZsAWxE9SgXMvg7xcE4Hsm7T6LGY",
//      },
//      votes: [
//        {
//          voter: {
//            name:
//              "CN=csl.cordite.foundation, O=Cordite Society Limited, L=London, C=GB",
//            owningKey:
//              "GfHq2tTVk9z4eXgyKhLHo2VzfnXBh86irZsAWxE9SgXMvg7xcE4Hsm7T6LGY",
//          },
//          voteType: { type: "DOWN" },
//        },
//      ],
//      members: [
//        {
//          name:
//            "CN=csl.cordite.foundation, O=Cordite Society Limited, L=London, C=GB",
//          owningKey:
//            "GfHq2tTVk9z4eXgyKhLHo2VzfnXBh86irZsAWxE9SgXMvg7xcE4Hsm7T6LGY",
//        },
//        {
//          name: "OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",
//          owningKey:
//            "GfHq2tTVk9z4eXgyMweZZkTHhFnbBufGxiGC6fu7sbpK5VwHm921Z7XkNmRt",
//        },
//      ],
//      daoKey: {
//        name: "plutus",
//        uuid: "030fdb80-5e56-4969-a884-9f71eda32847",
//        uniqueIdentifier: {
//          externalId: "plutus",
//          id: "030fdb80-5e56-4969-a884-9f71eda32847",
//        },
//      },
//      lifecycleState: "OPEN",----------------------
//      voteResult: { result: "VOTE_STILL_OPEN" }, ---------------------------------
//      voters: [
//        {
//          name:
//            "CN=csl.cordite.foundation, O=Cordite Society Limited, L=London, C=GB",
//          owningKey:
//            "GfHq2tTVk9z4eXgyKhLHo2VzfnXBh86irZsAWxE9SgXMvg7xcE4Hsm7T6LGY",
//        },
//      ],
//    },
//  ];

//----------------------Dao State-------------------------------------
// console.log(`\ndao: ${daoState.name}`);
// console.log(`\tPlutusModelData:`);
// const pmd = daoState.modelDataMap["io.cordite.dao.plutus.PlutusModelData"];
// console.log(`\t\tcurrentMintingRate: ${pmd.currentMintingRate}`);
// console.log(`\t\ttotalIssued: ${pmd.totalIssued}`);
// console.log(`\t\tminNextProposal: ${pmd.minNextProposal}`);
// console.log(`\t\tproposalPeriod: ${pmd.proposalPeriod}`);
// console.log(`\t\tplutusDaoOwner: ${pmd.plutusDaoOwner}`);???
// console.log(`\t\taccountName: ${pmd.accountName}`);
// console.log(`\t\tIssuableTokens:`);
// console.log(`\tMembershipModelData:`);
// const mmd =
//   daoState.modelDataMap["io.cordite.dao.membership.MembershipModelData"];
// console.log(`\t\tminimumMemberCount: ${mmd.minimumMemberCount}`);
// console.log(`\t\thasMinNumberOfMembers: ${mmd.hasMinNumberOfMembers}`);
// console.log(`\t\toneMemberPerOrg: ${mmd.oneMemberPerOrg}`);???
// console.log(`\tMembers:`);
// daoState.members.forEach((member) => console.log(`\t\t${member.name}`));
// {
//   name: "plutus",--------------------------
//   members: [-----------------------------------
//     {
//       name:
//         "CN=csl.cordite.foundation, O=Cordite Society Limited, L=London, C=GB",
//       owningKey: "GfHq2tTVk9z4eXgyNTrtTtFKVfZVoctggXT97cBTR9onSw7VihKHedN9DVX2",
//     },
//     {
//       name: "OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",
//       owningKey: "GfHq2tTVk9z4eXgyHusMrMpSb8J79UgNCTkPq5MXN3LAfCizCKQ4AZ1K1NF5",
//     },
//   ],
//   daoKey: {
//     name: "plutus",
//     uuid: "63fb3fe1-a9de-4391-8be4-a9b573bc6a3f",
//     uniqueIdentifier: {
//       externalId: "plutus",
//       id: "63fb3fe1-a9de-4391-8be4-a9b573bc6a3f",
//     },
//   },
//   modelDataMap: {
//     "io.cordite.dao.membership.MembershipModelData": {
//       "@class": "io.cordite.dao.membership.MembershipModelData",
//       membershipKey: {
//         name: "plutus",
//         uuid: "799f3623-85d4-4b59-9f3c-8e9ac8847eae",
//         uniqueIdentifier: {
//           externalId: "plutus",
//           id: "799f3623-85d4-4b59-9f3c-8e9ac8847eae",
//         },
//       },
//       minimumMemberCount: 1,---------------------
//       hasMinNumberOfMembers: true,---------------------------
//       strictMode: true,
//     },
//     "io.cordite.dao.voting.VotingModelData": {
//       "@class": "io.cordite.dao.voting.VotingModelData",
//       votingStrategySelector: {
//         "@class": "io.cordite.dao.plutus.PlutusVotingStrategySelector",
//       },
//     },
//     "io.cordite.dao.plutus.PlutusModelData": {
//       "@class": "io.cordite.dao.plutus.PlutusModelData",
//       accountName: "plutus",------------------------
//       issuableTokens: [ --------------------------
//         {
//           symbol: "XCD-1603307001",
//           exponent: 2,
//           issuerName:
//             "CN=csl.cordite.foundation, O=Cordite Society Limited, L=London, C=GB",
//         },
//       ],
//       currentMintingRate: 0.14, ----------------------------------
//       totalIssued: 0,-------------------------------------
//       minNextProposal: "2020-10-21T19:03:22.498Z",---------------------------
//       proposalPeriod: "PT672H",------------------------
//     },
//     "io.cordite.dao.proposal.ProposalModelData": {
//       "@class": "io.cordite.dao.proposal.ProposalModelData",
//       ordinaryVoteThreshold: 0.5,
//       extraordinaryVoteThreshold: 0.75,
//     },
//   },
//   participants: [
//     {
//       name:
//         "CN=csl.cordite.foundation, O=Cordite Society Limited, L=London, C=GB",
//       owningKey: "GfHq2tTVk9z4eXgyNTrtTtFKVfZVoctggXT97cBTR9onSw7VihKHedN9DVX2",
//     },
//     {
//       name: "OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",
//       owningKey: "GfHq2tTVk9z4eXgyHusMrMpSb8J79UgNCTkPq5MXN3LAfCizCKQ4AZ1K1NF5",
//     },
//   ],
// };
