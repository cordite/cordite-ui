import { call, put } from "redux-saga/effects";
import { setAlert } from "../actions/alertActions";
import { setLogin } from "../actions/loginActions";
import { IAlert } from "../reducers/alertReducer";
import { ApplicationConfig } from "../../ApplicationConfig";
import { updateBraidProxy } from "../actions/proxyActions";
import { getProposals } from "../actions/daoActions";
import { getAccountBalance, updateAccount } from "../actions/braidActions";
import { AppState } from "../reducers";
import { Store } from "../../index";
import { ITransactionPayload } from "../reducers/transactionsReducer";
import { getTransactions } from "../actions/transactionsActions";

const BraidProxy = require("braid-client").Proxy;

let braidProxy: any;

export function initProxy(url: string, credentials: any): Promise<any> {
  if (!braidProxy) {
// <<<<<<< HEAD
//     return new BraidProxy(
//       { url: url, credentials: credentials },
//       onOpen,
//       onClose,
//       onError,
//       { strictSSL: false }
//     );
// =======
    const transportConfig = {
      transports: 'xhr-polling'
    };
    
    return new BraidProxy({ url: url, credentials: credentials }, onOpen, onClose, onError, transportConfig);
//>>>>>>> master
  } else {
    return Promise.resolve(braidProxy);
  }
}

function* initBraidProxySaga(action: any) {
  try {
    if (!braidProxy) {
      console.log("proxy dont exist");
      braidProxy = yield call(
        initProxy,
        ApplicationConfig.singleton.apiUrl,
        action.payload
      );
      // console.log(JSON.stringify(braidProxy));
    }
    // yield call(braidProxy.ledger.listAccounts);
  } catch (err) {
    const alert: IAlert = {
      msg: "something went wrong try to login again",
      alertType: "error",
    };
    // braidProxy = null;
    yield put(setAlert(alert));
    yield put(setLogin(false));
  }
}

function* removeBraidProxySaga() {
  try {
    braidProxy = null;
    yield put(updateBraidProxy(null));
    const alert: IAlert = {
      msg: "Logout Successful",
      alertType: "success",
    };
    yield put(setAlert(alert));
    yield put(setLogin(false));
  } catch (err) {
    const alert: IAlert = {
      msg: "can't logout please refresh your browser...",
      alertType: "error",
    };
    yield put(setAlert(alert));
  }
}
//===================================================================
//===================================================================
//===================================================================
let updates = 0;
let proposalUpdates = 0;
let transactionUpdates = 0;

function onOpen() {
  // console.log("opened");
  if (braidProxy) {
    Store.dispatch(updateBraidProxy(braidProxy));
    Store.dispatch(setLogin(true));

    const alert: IAlert = {
      msg: "Login Successful",
      alertType: "success",
    };
    Store.dispatch(setAlert(alert));
  } else {
    braidProxy = null;
    updateBraidProxy(null);
  }
  subscribeToProposalUpdates();
  subscribeToTransactionUpdates();
}

function subscribeToProposalUpdates() {
  let tempUpdate: any = updates;
  updates = braidProxy.dao.listenForProposalUpdates().then((res: any) => {
    proposalUpdates = proposalUpdates + 1;
    // console.log("Proposal update", proposalUpdates, "\n", res);
    // console.log(JSON.stringify(res, null, 2));
    subscribeToProposalUpdates();
  });

  if (updates !== tempUpdate) {
    // console.log("inside proposal updates if statement");
    Store.dispatch(getProposals());
  }
}

export const getAccountId = (state: AppState) => state.accountBalance.accountId;

function subscribeToTransactionUpdates() {
  // console.log("subscribing to transactions");
  updates = braidProxy.ledger.listenForTransactions([]).then((res: any) => {
    transactionUpdates = transactionUpdates + 1;
    // console.log("transaction up", transactionUpdates, "\n", res);
    subscribeToTransactionUpdates();
  });

  const state = Store.getState();
  // console.log("state = ", JSON.stringify(state));
  const accId = state.accountBalance.accountId;
  // console.log("Account Id = ", accId);
  const payload: ITransactionPayload = {
    accountId: accId,
    page: {
      pageNumber: 1,
      pageSize: 10,
    },
  };
  Store.dispatch(getTransactions(payload));
  Store.dispatch(getAccountBalance(accId));
  Store.dispatch(updateAccount());
}

function onClose() {
  // console.log("closed");
}

function onError(e: any) {
  braidProxy = null;
  Store.dispatch(updateBraidProxy(null));
  const alert: IAlert = {
    msg: "Credentials do not match try to login again",
    alertType: "error",
  };
  Store.dispatch(setAlert(alert));
  Store.dispatch(setLogin(false));
  // console.error("error", e);
}

export { initBraidProxySaga, removeBraidProxySaga, braidProxy };
