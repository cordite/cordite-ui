import { takeEvery, all, takeLatest } from "redux-saga/effects";
import * as types from "../types";
import {
  createAccountSaga,
  updateAccountBalanceSaga,
  updateAccountSaga,
  getTokenListSaga,
  transferTokenSaga,
  getTransactionsSaga,
} from "./braidSaga";
import { setAlertSaga } from "./alertSaga";
import {
  getDaoInfoSaga,
  getProposalsSaga,
  voteDownProposalSaga,
  voteUpProposalSaga,
} from "./daoSaga";
import { nodeInfoSaga } from "./nodeInfoSaga";
import { initBraidProxySaga, removeBraidProxySaga } from "./proxySaga";
//exporting all the saga functions
export default function* rootSaga() {
  yield all([
    //Braid Account sagas
    takeEvery(types.BRAID_CREATE_ACCOUNT, createAccountSaga),
    takeEvery(types.BRAID_UPDATE_ACCOUNT, updateAccountSaga),
    takeEvery(types.BRAID_GET_ACCOUNT_BALANCE, updateAccountBalanceSaga),
    takeEvery(types.BRAID_GET_TOKEN_LIST, getTokenListSaga),
    takeEvery(types.BRAID_TRANSFER_TOKEN, transferTokenSaga),
    //Alert sagas
    takeEvery(types.SET_ALERT, setAlertSaga),
    takeEvery(types.BRAID_GET_TRANSACTIONS, getTransactionsSaga),
    takeEvery(types.GET_PROPOSALS, getProposalsSaga),
    takeEvery(types.VOTE_DOWN, voteDownProposalSaga),
    takeEvery(types.VOTE_UP, voteUpProposalSaga),
    takeEvery(types.GET_NODE_INFO, nodeInfoSaga),
    takeEvery(types.GET_DAO_INFO, getDaoInfoSaga),
    takeLatest(types.INIT_BRAID_PROXY, initBraidProxySaga),
    takeLatest(types.REMOVE_BRAID_PROXY, removeBraidProxySaga),
    // takeLatest(types.GET_PROPOSAL_UPDATE, subscribeToProposalSaga),
  ]);
}

// ledger.listTokenTypes()

// [
//   {
//    symbol: 'R2C',
//    exponent: 2,
//    description: '',
//    issuer: {
//     name: 'OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US',
//     owningKey: 'GfHq2tTVk9z4eXgyHTBh1XWofFvJwzAkT35DLwKtNo6By9zWzhSMGCvqmbc6'
//    },
//    linearId: { externalId: 'R2C', id: '02c7783c-f597-4f59-b3ec-fd35ed9ffc20' },
//    metaData: null,
//    settlements: [],
//    participants: [ [Object] ],
//    descriptor: {
//     symbol: 'R2C',
//     issuerName: 'OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US',
//     uri: 'R2C:OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US'
//    }
//   },
//   {
//    symbol: 'R3C',
//    exponent: 2,
//    description: '',
//    issuer: {
//     name: 'OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US',
//     owningKey: 'GfHq2tTVk9z4eXgyHTBh1XWofFvJwzAkT35DLwKtNo6By9zWzhSMGCvqmbc6'
//    },
//    linearId: { externalId: 'R3C', id: '5d1ef05e-e45b-4c6d-9eb8-d3db2c0644e0' },
//    metaData: null,
//    settlements: [],
//    participants: [ [Object] ],
//    descriptor: {
//     symbol: 'R3C',
//     issuerName: 'OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US',
//     uri: 'R3C:OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US'
//    }
//   }
//  ]
