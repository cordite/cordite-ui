import { call, put } from "redux-saga/effects";
import { braidProxy } from "./proxySaga";
import { setAlert } from "../actions/alertActions";
import { IAlert } from "../reducers/alertReducer";
import { INodeInfoState } from "../reducers/nodeInfoReducer";
import { setNodeInfo } from "../actions/nodeInfoActions";

// const notary = "O=Notary, OU=Cordite Foundation, L=London, C=GB";

function* nodeInfoSaga() {
  try {
    const res = yield call(braidProxy.network.myNodeInfo);

    let result: any = {};
    let match = res?.legalIdentities[0].name.split(",");

    match.forEach((some: any) => {
      var [k, v] = some.split("=");
      result[k] = v;
    });
    // console.log("object = ", result[" O"]);

    let payload: INodeInfoState = {
      organization: result[" O"],
      organizationUnit: result["OU"],
      country: result[" C"],
      city: result[" L"],
      fullName: res.legalIdentities[0].name,
    };
    yield put(setNodeInfo(payload));
    // const alert: IAlert = {
    //   msg: "successfully got node info",
    //   alertType: "success",
    // };
    // yield put(setAlert(alert));

    // console.log("Name is = ", JSON.stringify(res.legalIdentities[0].name));
  } catch (error) {
    const alert: IAlert = {
      msg: error.message,
      alertType: "error",
    };
    yield put(setAlert(alert));
  }
}

export { nodeInfoSaga };
