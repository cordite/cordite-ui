import { call, put } from "redux-saga/effects";
import { braidProxy } from "./proxySaga";
//import { IAccountState, initialAccountState, IBalanceState, initialBalanceState} from '../reducers/braidReducer';
import {
  updateAccount,
  updateAccountBalance,
  updateAccountList,
  updateTokenList,
} from "../actions/braidActions";
import { IBalanceState } from "../reducers/braidReducer";
import { ITokenState, IToken } from "../reducers/tokenReducer";
import { setAlert } from "../actions/alertActions";
import { IAlert } from "../reducers/alertReducer";
import {
  ITransaction,
  ITransactionPayload,
  ITransactionsState,
} from "../reducers/transactionsReducer";
import { updateTransactions } from "../actions/transactionsActions";
import { ApplicationConfig } from "../../ApplicationConfig";

// let notary = ApplicationConfig.singleton.notaryName;

function* createAccountSaga(action: any) {
  try {
    yield call(
      braidProxy.ledger.createAccount,
      action.payload.accountId,
      ApplicationConfig.singleton.notaryName
    );

    yield put(updateAccount());
  } catch (error) {
    const alert: IAlert = {
      msg: error.message,
      alertType: "error",
    };
    yield put(setAlert(alert));
  }
}

function* updateAccountSaga() {
  try {
    const accountList = yield call(braidProxy.ledger.listAccounts);

    const accounts: [string] = [""];

    accountList.map((acc: any) => accounts.push(acc.address.accountId));

    yield put(updateAccountList(accounts));
  } catch (error) {
    const alert: IAlert = {
      msg: error.message,
      alertType: "error",
    };
    yield put(setAlert(alert));
  }
}

function* updateAccountBalanceSaga(action: any) {
  try {
    const balances = yield call(
      braidProxy.ledger.balanceForAccount,
      action.payload
    );
    // console.log(JSON.stringify(balances));
    const balancesPayload: IBalanceState = {
      accountId: action.payload,
      balances: balances,
    };
    yield put(updateAccountBalance(balancesPayload));
  } catch (error) {
    const alert: IAlert = {
      msg: error.message,
      alertType: "error",
    };
    yield put(setAlert(alert));
  }
}

function* getTokenListSaga() {
  try {
    const tokensList = yield call(braidProxy.ledger.listTokenTypes);
    // console.log("token list = ", JSON.stringify(tokensList));

    var tokenResponse: IToken[] = [];

    tokensList.forEach((token: any) => {
      const tokenSingleton: IToken = {
        uri: token.descriptor.uri,
        tokenType: token.descriptor.symbol,
      };
      tokenResponse = [...tokenResponse, tokenSingleton];
    });

    const tokenPayload: ITokenState = {
      tokens: tokenResponse,
    };
    // console.log(JSON.stringify(tokenResponse));

    yield put(updateTokenList(tokenPayload));
  } catch (error) {
    const alert: IAlert = {
      msg: error.message,
      alertType: "error",
    };
    yield put(setAlert(alert));
  }
}

function* transferTokenSaga(action: any) {
  try {
    yield call(
      braidProxy.ledger.transferAccountToAccount,
      action.payload.amt,
      action.payload.uri,
      action.payload.fromAcc,
      action.payload.toAcc,
      action.payload.description,
      ApplicationConfig.singleton.notaryName
    );
    const alert: IAlert = {
      msg: "Transaction was successful!",
      alertType: "success",
    };
    yield put(setAlert(alert));
  } catch (error) {
    console.log(error);
    const alert: IAlert = {
      msg: error.message,
      alertType: "error",
    };
    yield put(setAlert(alert));
  }
}

function* getTransactionsSaga(action: any) {
  const payload: ITransactionPayload = {
    accountId: action.payload.accountId,
    page: {
      pageNumber: action.payload.page.pageNumber,
      pageSize: action.payload.page.pageSize,
    },
  };
  // console.log("In Transactions");
  try {
    const transactions = yield call(
      braidProxy.ledger.transactionsForAccount,
      payload.accountId,
      { pageNumber: payload.page.pageNumber, pageSize: payload.page.pageSize }
    );

    var transactionsArr: ITransaction[] = [];
    // console.log("transactions Payload = ", JSON.stringify(transactions));

    transactions.forEach((transaction: any) => {
      const amounts_zero = transaction.amounts[0];
      const amounts_one = transaction.amounts[1];
      if (transaction.command === "Issue") {
        const transactionPayload: ITransaction = {
          date: transaction.transactionTime, //transaction.transactionTime
          token_0: amounts_zero.amount.amountType.symbol, //transaction.amounts[0].amount.amountType.symbol
          amount_0: amounts_zero.amount.quantity, //transaction.amounts[0].amount.quantity
          type: transaction.command, //transaction.command
          acc_0_id: amounts_zero.accountAddress.accountId, //transaction.amounts[0].accountAddress.accountId
          acc_0_uri: amounts_zero.accountAddress.uri,
          description: transaction.description, //transaction.description
        };
        // console.log("transaction: ", JSON.stringify(transactionPayload));

        transactionsArr = [...transactionsArr, transactionPayload];
      } else if (transaction.command === "Move") {
        const transactionPayload: ITransaction = {
          date: transaction.transactionTime, //transaction.transactionTime
          type: transaction.command, //transaction.command
          description: transaction.description, //transaction.description
          token_0: amounts_zero.amount.amountType.symbol, //transaction.amounts[0].amount.amountType.symbol
          token_1: amounts_one.amount.amountType.symbol, //transaction.amounts[1].amount.amountType.symbol
          acc_0_id: amounts_zero.accountAddress.accountId, //transaction.amounts[0].accountAddress.accountId
          acc_1_id: amounts_one.accountAddress.accountId, //transaction.amounts[1].accountAddress.accountId;
          acc_0_uri: amounts_zero.accountAddress.uri, //transaction.amounts[0].accountAddress.uri;
          acc_1_uri: amounts_one.accountAddress.uri, //transaction.amounts[1].accountAddress.uri;
          amount_0: amounts_zero.amount.quantity, //transaction.amounts[0].amount.quantity
          amount_1: amounts_one.amount.quantity, //transaction.amounts[1].amount.quantity
        };
        // console.log("transaction: ", JSON.stringify(transactionPayload));

        transactionsArr = [...transactionsArr, transactionPayload];
      }
    });

    const updatePayload: ITransactionsState = {
      transactions: transactionsArr,
    };
    yield put(updateTransactions(updatePayload));

    // const alert: IAlert = {
    //   msg: "Fetched transactions successfully ",
    //   alertType: "success",
    // };
    // yield put(setAlert(alert));
  } catch (error) {
    console.log(error);
    const alert: IAlert = {
      msg: error.message,
      alertType: "error",
    };
    yield put(setAlert(alert));
  }
}

export {
  transferTokenSaga,
  getTokenListSaga,
  updateAccountBalanceSaga,
  updateAccountSaga,
  createAccountSaga,
  getTransactionsSaga,
};

//  [
//    {
//      participants: [
//        {
//          name: "OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",
//          owningKey:
//            "GfHq2tTVk9z4eXgyUpeDcAaiJJwYSBbi5pn99No8gC2NqGZKQk94q6ynLw6N",
//        },
//      ],
//      command: "Move",
//      amounts: [
//        {
//          accountAddress: {
//            accountId: "ac2",
//            party:
//              "OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",
//            uri:
//              "ac2@OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",
//          },
//          amount: {
//            quantity: "-5.00",
//            amountType: {
//              symbol: "R3C",
//              issuerName:
//                "OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",
//              uri:
//                "R3C:OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",
//            },
//          },
//        },
//        {
//          accountAddress: {
//            accountId: "ac1",
//            party:
//              "OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",
//            uri:
//              "ac1@OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",
//          },
//          amount: {
//            quantity: "5.00",
//            amountType: {
//              symbol: "R3C",
//              issuerName:
//                "OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",
//              uri:
//                "R3C:OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",
//            },
//          },
//        },
//      ],
//      description: "temp",
//      transactionTime: "2020-10-12T06:55:15.654Z",
//      transactionId: "2pbDkQMkEXthnraEpPeNp2qt3YmQGUnsd7oTs9UXLZsG",
//    },
//    {
//      participants: [
//        {
//          name: "OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",
//          owningKey:
//            "GfHq2tTVk9z4eXgyUpeDcAaiJJwYSBbi5pn99No8gC2NqGZKQk94q6ynLw6N",
//        },
//      ],
//      command: "Move",
//      amounts: [
//        {
//          accountAddress: {
//            accountId: "ac1",
//            party:
//              "OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",
//            uri:
//              "ac1@OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",
//          },
//          amount: {
//            quantity: "-5.00",
//            amountType: {
//              symbol: "R3C",
//              issuerName:
//                "OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",
//              uri:
//                "R3C:OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",
//            },
//          },
//        },
//        {
//          accountAddress: {
//            accountId: "ac2",
//            party:
//              "OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",
//            uri:
//              "ac2@OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",
//          },
//          amount: {
//            quantity: "5.00",
//            amountType: {
//              symbol: "R3C",
//              issuerName:
//                "OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",
//              uri:
//                "R3C:OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",
//            },
//          },
//        },
//      ],
//      description: "new",
//      transactionTime: "2020-10-12T06:03:13.116Z",
//      transactionId: "FBWhXbRcC8emCseCogkRGoEvgH7WtTpJQzwTTKFsMEEY",
//    },
//    {
//      participants: [
//        {
//          name: "OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",
//          owningKey:
//            "GfHq2tTVk9z4eXgyUpeDcAaiJJwYSBbi5pn99No8gC2NqGZKQk94q6ynLw6N",
//        },
//      ],
//      command: "Move",
//      amounts: [
//        {
//          accountAddress: {
//            accountId: "ac2",
//            party:
//              "OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",
//            uri:
//              "ac2@OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",
//          },
//          amount: {
//            quantity: "-10.00",
//            amountType: {
//              symbol: "R3C",
//              issuerName:
//                "OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",
//              uri:
//                "R3C:OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",
//            },
//          },
//        },
//        {
//          accountAddress: {
//            accountId: "ac1",
//            party:
//              "OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",
//            uri:
//              "ac1@OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",
//          },
//          amount: {
//            quantity: "10.00",
//            amountType: {
//              symbol: "R3C",
//              issuerName:
//                "OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",
//              uri:
//                "R3C:OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",
//            },
//          },
//        },
//      ],
//      description: "new",
//      transactionTime: "2020-10-12T06:01:44.141Z",
//      transactionId: "2uA75nJWURaDgd3w9dKmEvMFJCFwDxZQF9Abfv6w271A",
//    },
//    {
//      participants: [
//        {
//          name: "OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",
//          owningKey:
//            "GfHq2tTVk9z4eXgyUpeDcAaiJJwYSBbi5pn99No8gC2NqGZKQk94q6ynLw6N",
//        },
//      ],
//      command: "Issue",
//      amounts: [
//        {
//          accountAddress: {
//            accountId: "ac1",
//            party:
//              "OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",
//            uri:
//              "ac1@OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",
//          },
//          amount: {
//            quantity: "10.00",
//            amountType: {
//              symbol: "R3C",
//              issuerName:
//                "OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",
//              uri:
//                "R3C:OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",
//            },
//          },
//        },
//      ],
//      description: "you got money",
//      transactionTime: "2020-10-12T06:00:15.867Z",
//      transactionId: "27SjThX8YgMrSeXLU9TDqFbi9KmVUhPB4udBvupqc5B8",
//    },
//  ];
