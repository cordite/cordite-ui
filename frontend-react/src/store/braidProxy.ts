import { ApplicationConfig } from "../ApplicationConfig";
import { braidProxy } from "./sagas/proxySaga";
const BraidProxy = require("braid-client").Proxy;

// export async function initProxy(credentials: any): Promise<any> {
//   if (!braidProxy) {
//     return new Promise((resolve, reject) => {
//       const p = new BraidProxy(
//         { url: ApplicationConfig.singleton.apiUrl, credentials: credentials },
//         () => {
//           braidProxy = p;
//           resolve(braidProxy);
//         },
//         () => {
//           console.log("braid connection closed");
//         },
//         (err: any) => {
//           console.error(`failed to connect: ${err}`);
//           reject(err);
//         },
//         {
//           strictSSL: false,
//         }
//       );
//     });
//   } else {
//     return Promise.resolve(braidProxy);
//   }
// }
