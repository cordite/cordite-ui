import {
  makeStyles,
  createStyles,
  Theme,
  Typography,
  Card,
  CardContent,
  Chip,
  List,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
  Divider,
} from "@material-ui/core";
import React from "react";
import { green, pink } from "@material-ui/core/colors";
import { useSelector } from "react-redux";

import { AppState } from "../../store/reducers";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    center: {
      textAlign: "center",
      margin: "0 auto",
    },
    backgroundPrimary: {
      backgroundColor: "#546e7a",
      color: "#fff",
      padding: 10,
      borderRadius: 10,
    },
    root: {
      minWidth: 275,
      width: "100%",
    },
    mB30: {
      marginBottom: 30,
    },
    title: {
      fontSize: 14,
    },
    pos: {
      marginBottom: 12,
    },
    bullet: {
      display: "inline-block",
      margin: "0 2px",
      transform: "scale(0.8)",
    },
    active: {
      color: green[500],
    },
    voteDown: {
      color: pink[400],
    },
    listItem: {
      padding: 10,
    },
    margin: {
      marginBottom: 20,
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
      fontWeight: theme.typography.fontWeightRegular,
    },
  })
);

const PlutusDaoState: React.FC = () => {
  const classes = useStyles();

  const { plutusDaoState } = useSelector((state: AppState) => state.daoState);

  const getDate = () => {
    if (plutusDaoState?.pmd.minNextProposal) {
      const date: Date = new Date(plutusDaoState!.pmd.minNextProposal);
      return date.toUTCString();
    }
    return "";
  };

  const getCurrentMintingRate = () => {
    if (plutusDaoState?.pmd.minNextProposal) {
      const mintingRate = plutusDaoState!.pmd.currentMintingRate;
      return (mintingRate * 100).toFixed(2);
    }
    return "";
  };

  return (
    <React.Fragment>
      <Typography variant="h6" color="textSecondary" gutterBottom>
        Plutus Dao State
      </Typography>
      <Card className={`${classes.root} ${classes.mB30}`}>
        <CardContent>
          <List dense className={classes.root}>
            <ListItem className={classes.listItem}>
              <ListItemText primary={`Dao Creator`} />
              <ListItemSecondaryAction>
                <Chip
                  label={plutusDaoState?.pmd.plutusDaoOwner}
                  color="primary"
                />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
            <ListItem className={classes.listItem}>
              <ListItemText primary={`Current Minting Rate`} />
              <ListItemSecondaryAction>
                <Chip label={`${getCurrentMintingRate()} %`} color="primary" />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
            <ListItem className={classes.listItem}>
              <ListItemText primary={`Token in circulation`} />
              <ListItemSecondaryAction>
                <Chip label={plutusDaoState?.pmd.totalIssued} color="primary" />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
            <ListItem className={classes.listItem}>
              <ListItemText primary={`Next Vote Date`} />
              <ListItemSecondaryAction>
                <Chip label={getDate()} color="primary" />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
            <ListItem className={classes.listItem}>
              <ListItemText primary={`Members`} />
              <ListItemSecondaryAction>
                <Chip label={plutusDaoState?.members?.length} color="primary" />
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
            <Divider />
          </List>
        </CardContent>
      </Card>
    </React.Fragment>
  );
};
export default PlutusDaoState;
