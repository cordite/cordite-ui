import {
  makeStyles,
  createStyles,
  Theme,
  Typography,
  Button,
  Card,
  CardContent,
  Chip,
  List,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
  Divider,
  ButtonGroup,
  Accordion,
  AccordionDetails,
  AccordionSummary,
} from "@material-ui/core";
import React, { Dispatch } from "react";
import { green, pink } from "@material-ui/core/colors";
import { useDispatch, useSelector } from "react-redux";
import {
  IDaoActions,
  voteDownProposal,
  voteUpProposal,
} from "../../store/actions/daoActions";
import ThumbDown from "@material-ui/icons/ThumbDown";
import ThumbUp from "@material-ui/icons/ThumbUp";
import { AppState } from "../../store/reducers";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    center: {
      textAlign: "center",
      margin: "0 auto",
    },
    backgroundPrimary: {
      backgroundColor: "#546e7a",
      color: "#fff",
      padding: 10,
      borderRadius: 10,
    },
    root: {
      minWidth: 275,
      width: "100%",
    },
    title: {
      fontSize: 14,
    },
    pos: {
      marginBottom: 12,
    },
    bullet: {
      display: "inline-block",
      margin: "0 2px",
      transform: "scale(0.8)",
    },
    active: {
      color: green[500],
    },
    voteDown: {
      color: pink[400],
    },
    listItem: {
      padding: 10,
    },
    margin: {
      marginBottom: 20,
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
      fontWeight: theme.typography.fontWeightRegular,
    },
    accordian: {
      zIndex: 0,
    },
    voteStatus: {
      padding: 10,
    },
  })
);

const ActiveProposals: React.FC = () => {
  const classes = useStyles();
  const daoDispatch = useDispatch<Dispatch<IDaoActions>>();
  const { plutusDaoState } = useSelector((state: AppState) => state.daoState);
  // const { fullName } = useSelector((state: AppState) => state.nodeInfo);

  const {
    proposer,
    tokenType,
    lifeCycleState,
    basisPointChange,
    voteResult,
  } = useSelector((state: AppState) => state.daoState);

  const handleVoteUp = () => {
    daoDispatch(voteUpProposal());
    // checkCurrentVote();
  };
  const handleVoteDown = () => {
    daoDispatch(voteDownProposal());
    // checkCurrentVote();
  };

  //For current vote display
  // const [currentVote, setCurrentVote] = useState<string>("Not Voted Yet");

  // const checkCurrentVote = () => {
  //   const VOTED_DOWN = "Voted DOWN";
  //   const VOTED_UP = "Voted UP";
  //   const NOT_VOTED = "Not voted yet";

  //   plutusDaoState?.votes?.map((vote: any) => {
  //     if (vote.voter.name === fullName) {
  //       if (vote.voteType.type === "DOWN") return setCurrentVote(VOTED_DOWN);
  //       else if (vote.voteType.type === "UP") return setCurrentVote(VOTED_UP);
  //     }
  //   });
  //   return setCurrentVote(NOT_VOTED);
  // };

  // useEffect(() => {
  //   const checkCurrentVoteEffect = () => {
  //     const VOTED_DOWN = "Voted DOWN";
  //     const VOTED_UP = "Voted UP";
  //     const NOT_VOTED = "Not voted yet";

  //     plutusDaoState?.votes?.map((vote: any) => {
  //       if (vote.voter.name === fullName) {
  //         if (vote.voteType.type === "DOWN") return setCurrentVote(VOTED_DOWN);
  //         else if (vote.voteType.type === "UP") return setCurrentVote(VOTED_UP);
  //       }
  //     });
  //     return setCurrentVote(NOT_VOTED);
  //   };
  //   checkCurrentVoteEffect();
  // }, [fullName, plutusDaoState]);

  return (
    <React.Fragment>
      {proposer && (
        <React.Fragment>
          <Typography variant="h6" color="textSecondary" gutterBottom>
            Proposals
          </Typography>
          {lifeCycleState === "ACCEPTED" ? (
            <div>No proposals available</div>
          ) : (
            <Card className={classes.root}>
              <CardContent>
                <Typography
                  className={classes.title}
                  color="textSecondary"
                  gutterBottom
                >
                  Active Proposal
                </Typography>

                <List dense className={classes.root}>
                  <ListItem className={`${classes.listItem} ${classes.margin}`}>
                    <ListItemSecondaryAction>
                      <ButtonGroup
                        color="primary"
                        aria-label="outlined secondary button group"
                      >
                        <Button
                          startIcon={<ThumbUp />}
                          onClick={handleVoteUp}
                          className={classes.active}
                        >
                          Vote Up
                        </Button>

                        <Button
                          onClick={handleVoteDown}
                          startIcon={<ThumbDown />}
                          className={classes.voteDown}
                        >
                          Vote Down
                        </Button>
                      </ButtonGroup>
                      <br />
                      {/* <div className={classes.voteStatus}>
                      <Chip label={currentVote} />
                    </div> */}
                    </ListItemSecondaryAction>
                  </ListItem>
                  <ListItem className={classes.listItem}>
                    <ListItemText primary={`Proposer`} />
                    <ListItemSecondaryAction>
                      <Chip label={proposer} color="primary" />
                    </ListItemSecondaryAction>
                  </ListItem>
                  <Divider />
                  <ListItem className={classes.listItem}>
                    <ListItemText primary={`Token Type`} />
                    <ListItemSecondaryAction>
                      <Chip label={tokenType} color="primary" />
                    </ListItemSecondaryAction>
                  </ListItem>
                  <Divider />
                  <ListItem className={classes.listItem}>
                    <ListItemText primary={`Issuance Rate Basis Points`} />
                    <ListItemSecondaryAction>
                      <Chip label={basisPointChange} color="primary" />
                    </ListItemSecondaryAction>
                  </ListItem>
                  <Divider />
                  <ListItem className={classes.listItem}>
                    <ListItemText primary={`Life Cycle State`} />
                    <ListItemSecondaryAction>
                      <Chip label={lifeCycleState} color="primary" />
                    </ListItemSecondaryAction>
                  </ListItem>
                  <Divider />
                  <ListItem className={classes.listItem}>
                    <ListItemText primary={`Vote Result`} />
                    <ListItemSecondaryAction>
                      <Chip label={voteResult} color="primary" />
                    </ListItemSecondaryAction>
                  </ListItem>
                  <Divider />
                  <Accordion className={classes.accordian}>
                    <AccordionSummary
                      expandIcon={<ExpandMoreIcon />}
                      aria-controls="panel1a-content"
                      id="panel1a-header"
                    >
                      <Typography className={classes.heading}>
                        Vote List
                      </Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                      <List dense className={classes.root}>
                        <ListItem className={classes.listItem}>
                          <ListItemText primary={`Member`} />
                          <ListItemSecondaryAction>
                            <Chip label="Voted" variant="outlined" />
                          </ListItemSecondaryAction>
                        </ListItem>
                        <Divider />
                        {plutusDaoState?.votes?.map((vote: any) => (
                          <ListItem
                            key={vote.voter.name}
                            className={classes.listItem}
                          >
                            <ListItemText primary={vote.voter.name} />
                            <ListItemSecondaryAction>
                              <Chip
                                label={vote.voteType.type}
                                color="primary"
                              />
                            </ListItemSecondaryAction>
                          </ListItem>
                        ))}
                        <Divider />
                      </List>
                    </AccordionDetails>
                  </Accordion>
                </List>
              </CardContent>
            </Card>
          )}
        </React.Fragment>
      )}
    </React.Fragment>
  );
};
export default ActiveProposals;
