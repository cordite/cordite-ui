import {
  Grid,
  makeStyles,
  TableContainer,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Paper,
  Typography,
  createStyles,
  Theme,
  withStyles,
  Button,
} from "@material-ui/core";
import React, { Dispatch, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { AppState } from "../../store/reducers";
import {
  ITransactionsGetActions,
  getTransactions,
} from "../../store/actions/transactionsActions";
import { ITransactionPayload } from "../../store/reducers/transactionsReducer";
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      maxWidth: 200,
      marginRight: 20,
      minWidth: 150,
      flexGrow: 1,
    },
    iconColor: {
      color: "#fff",
    },
    marginTop30: {
      marginTop: 30,
    },
    marginTop20: {
      marginTop: 20,
    },
    marginRight20: {
      marginRight: 20,
    },
    padding20: {
      padding: 20,
    },
    gridItem: {
      minWidth: 115,
    },
    table: {
      maxWidth: 250,
    },
    tableTransactions: {
      width: 900,
    },
    paper: {
      backgroundColor: theme.palette.background.paper,
      boxShadow: theme.shadows[5],
      border: "2px solid #000",

      padding: theme.spacing(2, 4, 3),
    },
    modal: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
    },
    button: {
      margin: theme.spacing(1),
    },
    buttonContainer: {
      minWidth: "100%",
    },

    center: {
      width: "100%",
      alignContent: "center",
      justifyContent: "center",
      textAlign: "center",
    },
  })
);

const StyledTableCell = withStyles((theme: Theme) =>
  createStyles({
    head: {
      backgroundColor: theme.palette.primary.main,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
  })
)(TableCell);

const StyledTableRow = withStyles((theme: Theme) =>
  createStyles({
    root: { cursor: "pointer" },
  })
)(TableRow);

type props = {
  id?: string;
};
const Transactions: React.FC<props> = ({ id }) => {
  const classes = useStyles();
  const transactionsDispatch = useDispatch<Dispatch<ITransactionsGetActions>>();
  const [transactionsPagination, setTransactionsPagination] = useState(1);

  const handleNextPage = () => {
    const newPage: number = transactionsPagination + 1;
    setTransactionsPagination(newPage);
  };

  const handlePrevPage = () => {
    if (transactionsPagination > 1) {
      const newPage: number = transactionsPagination - 1;
      setTransactionsPagination(newPage);
    }
  };

  useEffect(() => {
    const payload: ITransactionPayload = {
      accountId: id,
      page: {
        pageNumber: transactionsPagination,
        pageSize: 10,
      },
    };

    transactionsDispatch(getTransactions(payload));
  }, [id, transactionsDispatch, transactionsPagination]);

  const { transactions } = useSelector(
    (state: AppState) => state.transactionList
  );
  const { accountId } = useSelector((state: AppState) => state.accountBalance);

  return (
    <React.Fragment>
      <Grid item style={{ marginLeft: 40 }}>
        <Typography className={classes.marginTop30} variant="h6">
          Transactions
        </Typography>

        <TableContainer component={Paper} className={classes.tableTransactions}>
          <Table
            className={classes.tableTransactions}
            aria-label="customized table"
          >
            <TableHead>
              <TableRow>
                <StyledTableCell>Date / Time</StyledTableCell>
                <StyledTableCell align="right">Token</StyledTableCell>
                <StyledTableCell align="right">Amount</StyledTableCell>
                <StyledTableCell align="right">Other Party</StyledTableCell>
                <StyledTableCell align="right">Type</StyledTableCell>
                <StyledTableCell align="right">Description</StyledTableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {transactions.map((transaction) =>
                accountId === transaction.acc_0_id ? (
                  <StyledTableRow key={transaction.date} hover={true}>
                    <StyledTableCell component="th" scope="row">
                      {transaction.date}
                    </StyledTableCell>
                    <StyledTableCell align="right">
                      {transaction.token_0}
                    </StyledTableCell>
                    <StyledTableCell align="right">
                      {transaction.amount_0}
                    </StyledTableCell>
                    <StyledTableCell align="right">
                      {transaction.acc_1_uri}
                    </StyledTableCell>
                    <StyledTableCell align="right">
                      {transaction.type}
                    </StyledTableCell>
                    <StyledTableCell align="right">
                      {transaction.description}
                    </StyledTableCell>
                  </StyledTableRow>
                ) : (
                  <StyledTableRow key={transaction.date} hover={true}>
                    <StyledTableCell component="th" scope="row">
                      {transaction.date}
                    </StyledTableCell>
                    <StyledTableCell align="right">
                      {transaction.token_1}
                    </StyledTableCell>
                    <StyledTableCell align="right">
                      {transaction.amount_1}
                    </StyledTableCell>
                    <StyledTableCell align="right">
                      {transaction.acc_0_uri}
                    </StyledTableCell>
                    <StyledTableCell align="right">
                      {transaction.type}
                    </StyledTableCell>
                    <StyledTableCell align="right">
                      {transaction.description}
                    </StyledTableCell>
                  </StyledTableRow>
                )
              )}
            </TableBody>

            {/* {transactions.length > 18 && (
              
            )} */}
          </Table>
        </TableContainer>
        <div className={`${classes.marginTop20} ${classes.center}`}>
          {transactionsPagination > 1 && (
            <Button
              className={classes.marginRight20}
              variant="contained"
              color="primary"
              onClick={handlePrevPage}
            >
              Previous Page
            </Button>
          )}
          {transactions.length > 0 ? (
            <Button
              variant="contained"
              color="primary"
              onClick={handleNextPage}
            >
              Next Page
            </Button>
          ) : (
            "No more transaction please go back to the previous page"
          )}
        </div>
      </Grid>
    </React.Fragment>
  );
};
export default Transactions;
