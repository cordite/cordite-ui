import {
  Avatar,
  Button,
  Card,
  Grid,
  ListItem,
  ListItemAvatar,
  ListItemText,
  makeStyles,
  createStyles,
  Theme,
  Backdrop,
  Fade,
  Modal,
  TextField,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
} from "@material-ui/core";
import React, { Dispatch, useEffect } from "react";
import AccountBalanceWalletIcon from "@material-ui/icons/AccountBalanceWallet";
import { blueGrey } from "@material-ui/core/colors";
import { useDispatch, useSelector } from "react-redux";
import { AppState } from "../../store/reducers";
import { ITransferToken } from "../../store/reducers/tokenReducer";
import {
  getTokenList,
  IBraidTokenActions,
  transferToken,
} from "../../store/actions/braidActions";
import { ApplicationConfig } from "../../ApplicationConfig";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      maxWidth: 200,
      marginRight: 20,
      minWidth: 150,
    },
    iconColor: {
      color: "#fff",
    },
    paper: {
      backgroundColor: theme.palette.background.paper,
      boxShadow: theme.shadows[5],
      border: blueGrey[600],
      padding: theme.spacing(2, 4, 3),
    },
    modal: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
    },
    margin: {
      margin: "10px",
    },
    center: {
      textAlign: "center",
      margin: "0 auto",
    },
    left: {
      textAlign: "left",
      margin: "0 auto",
    },
    colorPrimary: {
      color: blueGrey[600],
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 200,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  })
);

const TransferMoney: React.FC = () => {
  const classes = useStyles();
  const { accountId } = useSelector((state: AppState) => state.accountBalance);
  const [open, setOpen] = React.useState(false);
  const [form, setForm] = React.useState({
    recipientAdd: "",
    amount: 0,
    description: "",
  });
  const tokenDispatch = useDispatch<Dispatch<IBraidTokenActions>>();
  const [tokenType, setTokenType] = React.useState("");

  const { tokens } = useSelector((state: AppState) => state.tokenList);

  useEffect(() => {
    tokenDispatch(getTokenList(accountId));
  }, [accountId, tokenDispatch]);

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setTokenType(event.target.value as string);
    // console.log(tokenType);
  };

  const { recipientAdd, amount, description } = form;
  const handleInputChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ): void => {
    e.preventDefault();
    setForm({ ...form, [e.target.name]: e.target.value });
  };
  const handleSendMoney = () => {
    const sendMoney: ITransferToken = {
      amt: amount,
      fromAcc: accountId,
      toAcc: recipientAdd,
      description: description,
      notary: ApplicationConfig.singleton.notaryName,
      uri: tokenType,
    };
    tokenDispatch(transferToken(sendMoney));
    handleClose();
  };

  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  return (
    <React.Fragment>
      <Grid container>
        <Card className={classes.root}>
          <ListItem>
            <ListItemAvatar>
              <Avatar>
                <AccountBalanceWalletIcon className={classes.iconColor} />
              </Avatar>
            </ListItemAvatar>
            <ListItemText primary={accountId} />
          </ListItem>
        </Card>
        <Button variant="contained" color="primary" onClick={handleOpen}>
          Transfer Tokens
        </Button>
      </Grid>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition={true}
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <h2 className={classes.colorPrimary}>Transfer Token</h2>
            <form
              className={(classes.root, classes.center)}
              noValidate
              autoComplete="off"
            >
              <TextField
                className={classes.margin}
                color="primary"
                id="outlined-basic"
                label="Account Selected"
                variant="outlined"
                value={accountId}
                disabled={true}
              />
              <TextField
                className={classes.margin}
                color="primary"
                id="outlined-basic"
                label="Recipient Address"
                variant="outlined"
                name="recipientAdd"
                value={recipientAdd}
                onChange={handleInputChange}
                multiline={true}
              />
            </form>
            <form
              className={(classes.root, classes.left)}
              noValidate
              autoComplete="off"
            >
              <TextField
                className={classes.margin}
                color="primary"
                id="outlined-basic"
                label="Amount"
                variant="outlined"
                name="amount"
                value={amount}
                onChange={handleInputChange}
              />
              <TextField
                className={classes.margin}
                color="primary"
                id="outlined-basic"
                label="Description"
                variant="outlined"
                name="description"
                value={description}
                onChange={handleInputChange}
              />
            </form>
            <form
              className={(classes.root, classes.left)}
              noValidate
              autoComplete="off"
            >
              <FormControl variant="outlined" className={classes.formControl}>
                <InputLabel id="demo-simple-select-outlined-label">
                  Select Token
                </InputLabel>
                <Select
                  name="tokenType"
                  labelId="demo-simple-select-outlined-label"
                  id="demo-simple-select-outlined"
                  value={tokenType}
                  onChange={handleChange}
                  label="Select Token"
                >
                  <MenuItem value="">
                    <em>None</em>
                  </MenuItem>
                  {tokens.map((token) => (
                    <MenuItem key={token.tokenType} value={token.uri}>
                      {token.tokenType}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </form>
            <div className={classes.center}>
              <Button
                style={{ height: "55px" }}
                variant="contained"
                color="primary"
                onClick={handleSendMoney}
              >
                Send Tokens
              </Button>
            </div>
          </div>
        </Fade>
      </Modal>
    </React.Fragment>
  );
};
export default TransferMoney;
