import {
  Grid,
  makeStyles,
  TableContainer,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Paper,
  Typography,
  createStyles,
  Theme,
  withStyles,
} from "@material-ui/core";
import React, { Dispatch, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { AppState } from "../../store/reducers";
import {
  getAccountBalance,
  IBraidAccountBalanceActions,
} from "../../store/actions/braidActions";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      maxWidth: 200,
      marginRight: 20,
      minWidth: 150,
    },
    iconColor: {
      color: "#fff",
    },
    marginTop30: {
      marginTop: 30,
    },
    marginTop20: {
      marginTop: 20,
    },
    padding20: {
      padding: 20,
    },
    gridItem: {
      minWidth: 115,
    },
    table: {
      maxWidth: 250,
    },
    tableTransactions: {
      maxWidth: 600,
    },
    paper: {
      backgroundColor: theme.palette.background.paper,
      boxShadow: theme.shadows[5],
      border: "2px solid #000",

      padding: theme.spacing(2, 4, 3),
    },
    modal: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
    },
  })
);

const StyledTableCell = withStyles((theme: Theme) =>
  createStyles({
    head: {
      backgroundColor: theme.palette.primary.main,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
  })
)(TableCell);

const StyledTableRow = withStyles((theme: Theme) =>
  createStyles({
    root: { cursor: "pointer" },
  })
)(TableRow);

type props = {
  id?: string;
};
const AccountBalance: React.FC<props> = ({ id }) => {
  const classes = useStyles();

  //lifecycle methods
  const accountBalanceDispatch = useDispatch<
    Dispatch<IBraidAccountBalanceActions>
  >();
  useEffect(() => {
    accountBalanceDispatch(getAccountBalance(id));
  }, [id, accountBalanceDispatch]);

  const { balances } = useSelector((state: AppState) => state.accountBalance);

  return (
    <React.Fragment>
      <Grid item>
        <Typography className={classes.marginTop30} variant="h6">
          Balances
        </Typography>

        <TableContainer component={Paper} className={classes.table}>
          <Table className={classes.table} aria-label="customized table">
            <TableHead>
              <TableRow>
                <StyledTableCell>Token</StyledTableCell>
                <StyledTableCell align="right">Amount</StyledTableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {balances.map((balance) => (
                <StyledTableRow key={balance.amountType.symbol} hover={true}>
                  <StyledTableCell component="th" scope="row">
                    {balance.amountType.symbol}
                  </StyledTableCell>
                  <StyledTableCell align="right">
                    {balance.quantity}
                  </StyledTableCell>
                </StyledTableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
    </React.Fragment>
  );
};
export default AccountBalance;
