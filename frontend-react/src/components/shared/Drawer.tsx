import React, { Dispatch, Fragment } from "react";
import clsx from "clsx";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import HomeIcon from "@material-ui/icons/Home";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import ListItemText from "@material-ui/core/ListItemText";
import AccountBalanceIcon from "@material-ui/icons/AccountBalance";
import AccountBalanceWalletIcon from "@material-ui/icons/AccountBalanceWallet";
import HowToVoteIcon from "@material-ui/icons/HowToVote";
import { ExpandLess, ExpandMore } from "@material-ui/icons";
import { Button, Collapse } from "@material-ui/core";
import KeyboardBackspaceIcon from "@material-ui/icons/KeyboardBackspace";
import { Link, useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { AppState } from "../../store/reducers";
import {
  IBraidAccountActions,
  updateAccount,
} from "../../store/actions/braidActions";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import { ILoginActions, logoutUser } from "../../store/actions/loginActions";
const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(["width", "margin"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
    appBarShift: {
      marginLeft: drawerWidth,
      width: `calc(100% - ${drawerWidth}px)`,
      transition: theme.transitions.create(["width", "margin"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    menuButton: {
      marginRight: 36,
    },
    hide: {
      display: "none",
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
      whiteSpace: "nowrap",
    },
    drawerOpen: {
      width: drawerWidth,
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    toolbar: {
      display: "flex",
      alignItems: "center",
      justifyContent: "flex-end",
      padding: theme.spacing(0, 1),
      // necessary for content to be below app bar
      ...theme.mixins.toolbar,
    },
    drawerClose: {
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      overflowX: "hidden",
      width: theme.spacing(7) + 1,
      [theme.breakpoints.up("sm")]: {
        width: theme.spacing(9) + 1,
      },
    },
    nested: {
      paddingLeft: theme.spacing(4),
    },
    logoutButton: {
      position: "fixed",
      right: 10,
    },
  })
);
export default function MiniDrawer() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);
  const [listOpen, setListOpen] = React.useState(false);
  const { accounts } = useSelector((state: AppState) => state.accountList);
  const accountDispatch = useDispatch<Dispatch<IBraidAccountActions>>();
  const loginDispatch = useDispatch<Dispatch<ILoginActions>>();

  const history = useHistory();

  const handleListClick = () => {
    accountDispatch(updateAccount());

    setListOpen(!listOpen);
  };

  const handleLogout = () => {
    loginDispatch(logoutUser());
  };

  const handleDrawerOpen = () => {
    setOpen(true);
  };
  const handleDrawerClose = () => {
    setOpen(false);
  };

  function navigateToAccount(accountId: string) {
    history.push(`/dashboard/accounts/${accountId}`);
  }

  const navigateToDao = () => {
    history.push("/dashboard/dao");
  };

  const navigateToHome = () => {
    history.push("/dashboard/");
  };
  return (
    <Fragment>
      <CssBaseline />
      <AppBar
        color="primary"
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, {
              [classes.hide]: open,
            })}
          >
            <MenuIcon />
          </IconButton>

          <Typography variant="h6" noWrap={true}>
            Cordite
          </Typography>
          <Button
            variant="contained"
            color="secondary"
            className={classes.logoutButton}
            onClick={handleLogout}
          >
            Logout&nbsp;
            <ExitToAppIcon />
          </Button>
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
      >
        <div className={classes.toolbar}>
          <IconButton onClick={handleDrawerClose}>
            <KeyboardBackspaceIcon />
          </IconButton>
        </div>
        <Divider />
        <List>
          <ListItem button={true} onClick={navigateToHome}>
            <ListItemIcon>
              <HomeIcon color="primary" />
            </ListItemIcon>
            <ListItemText primary="Home" />
          </ListItem>
          <ListItem button={true} onClick={navigateToDao}>
            <ListItemIcon>
              <HowToVoteIcon color="primary" />
            </ListItemIcon>
            <ListItemText primary="DAO" />
          </ListItem>
          <ListItem button={true} onClick={handleListClick}>
            <ListItemIcon>
              <AccountBalanceIcon color="primary" />
            </ListItemIcon>
            <ListItemText primary="Accounts" />
            {listOpen ? (
              <ExpandLess color="primary" />
            ) : (
              <ExpandMore color="primary" />
            )}
          </ListItem>
          <Collapse in={listOpen} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
              {accounts.map(
                (account) =>
                  account && (
                    <ListItem
                      key={account}
                      button
                      className={classes.nested}
                      onClick={() => navigateToAccount(account)}
                    >
                      <ListItemIcon color="primary">
                        <AccountBalanceWalletIcon color="primary" />
                      </ListItemIcon>
                      <ListItemText primary={account} />
                    </ListItem>
                  )
              )}
              <Link to="/dashboard/create-account">
                <ListItem button className={classes.nested}>
                  <ListItemIcon style={{ margin: "0 auto" }}>
                    <AddCircleIcon color="primary" />
                  </ListItemIcon>
                </ListItem>
              </Link>
            </List>
          </Collapse>
        </List>
      </Drawer>
    </Fragment>
  );
}
