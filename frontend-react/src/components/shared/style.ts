import { createMuiTheme } from "@material-ui/core";
import { blueGrey } from "@material-ui/core/colors";

export const theme = createMuiTheme({
  palette: {
    primary: { main: blueGrey[600] },
    secondary: { main: "#da2c43" },
  },
});
