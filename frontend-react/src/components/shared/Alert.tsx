import { makeStyles, Theme, createStyles } from "@material-ui/core";
import React, { Fragment } from "react";
import Alert from "@material-ui/lab/Alert/Alert";
import { useSelector } from "react-redux";
import { AppState } from "../../store/reducers";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
    alert: {
      position: "fixed",
      bottom: 20,
      left: "30%",
      width: "50%",
      zIndex: 999,
    },
    alertPadding: {
      margin: 10,
    },
  })
);

const AlertComp: React.FC = () => {
  const classes = useStyles();

  const { alerts } = useSelector((state: AppState) => state.alertList);

  return (
    <Fragment>
      <div className={classes.alert}>
        {alerts.map(
          (alert) =>
            alert.id && (
              <Alert
                key={alert.id}
                className={classes.alertPadding}
                severity={alert.alertType}
              >
                {alert.msg}
              </Alert>
            )
        )}
      </div>
    </Fragment>
  );
};

export default AlertComp;
