import React from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      ...theme.typography.button,
      backgroundColor: theme.palette.background.paper,
      padding: theme.spacing(1),
    },
  })
);

const TempComp: React.FC = () => {
  const classes = useStyles();
  return (
    <div className="root" style={{ marginLeft: '20%' }}>
      Something
    </div>
  );
};

export default TempComp;
