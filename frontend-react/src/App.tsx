import { makeStyles, createStyles } from "@material-ui/core";
import React, { Fragment } from "react";
import Drawer from "./components/shared/Drawer";
import "./App.scss";
import { Route, Switch, Redirect } from "react-router-dom";
import Alert from "./components/shared/Alert";
//Pages
import Accounts from "./pages/Accounts";
import CreateAccount from "./pages/CreateAccount";
import Dao from "./pages/Dao";
import Login from "./pages/Login";
import { useSelector } from "react-redux";
import { AppState } from "./store/reducers";
import Home from "./pages/Home";
// import Home from "./pages/Home";
const useStyles = makeStyles(() =>
  createStyles({
    root: {
      display: "flex",
    },
  })
);
function App() {
  const classes = useStyles();
  const { login } = useSelector((state: AppState) => state.loginState);

  return (
    <div className={classes.root}>
      <Alert />
      {login && <Drawer />}

      <Fragment>
        <Switch>
          {login ? (
            <React.Fragment>
              <Route path="/dashboard/accounts/:id" component={Accounts} />
              <Route exact={true} path="/dashboard/dao" component={Dao} />
              <Route exact={true} path="/dashboard/" component={Home} />
              <Route
                exact={true}
                path="/dashboard/create-account"
                component={CreateAccount}
              />
              <Redirect from="**" to="/dashboard/" />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <Route exact path="/" component={Login} />
              <Redirect from="**" to="/" />
            </React.Fragment>
          )}
        </Switch>
      </Fragment>
    </div>
  );
}

export default App;
