# Cordite UI documentation

## Running this App

<details>

### Cordite Node

This app requires a Cordite Node to work with. Please refer to the [Cordite README](https://gitlab.com/cordite/cordite/-/blob/master/README.MD#how-do-i-deploy-my-own-cordite-node) to start one.

### Environment Variables

The application support a number of environment variables. These can be setup either when running the app natively on the command line, or via docker. 

| Command line                             | Docker                         | Default                                                              | Purpose                               |
| ---------------------------------------- | ------------------------------ | -------------------------------------------------------------------- | ------------------------------------- |
| REACT_APP_CORDITE_UI_CSL_NAME            | CORDITE_UI_NODE_API            | https://localhost:8081/api/                                          | Braid API endpoint                    |
| REACT_APP_CORDITE_UI_DEFAULT_NOTARY_NAME | CORDITE_UI_DEFAULT_NOTARY_NAME | OU=Cordite Foundation, O=Cordite Bootstrap Notary, L=London, C=GB    | Default Corda Notary for transactions |
| REACT_APP_CORDITE_UI_CSL_NAME            | CORDITE_UI_CSL_NAME            | CN=csl.cordite.foundation, O=Cordite Society Limited, L=London, C=GB | Default Cordite Society DAO Party     |

### Running with a node in a Cordite test network

```
DEFAULT_NOTARY="OU=Cordite Foundation, O=Notary, L=London, C=GB"
MY_NODE_API_URL="<protocol>://<host-and-port>/api/"
REACT_APP_CORDITE_UI_DEFAULT_NOTARY_NAME=${DEFAULT_NOTARY} REACT_APP_CORDITE_UI_NODE_API=${MY_NODE_API_URL} npm start
```

</details>

## Create-React documentation

<details>

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
</details>
