#!/bin/sh

echo "starting"

JSON_FMT='{"apiUrl":"%s","notaryName":"%s","cslName":"%s"}\n'
json=$(printf "$JSON_FMT" "$CORDITE_UI_NODE_API" "$CORDITE_UI_DEFAULT_NOTARY_NAME" "$CORDITE_UI_CSL_NAME")
echo $json >> /app/config.json
echo "created config: $(cat /app/config.json)"

