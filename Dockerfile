# pull official base image
FROM node:12.19.0-alpine as build

ENV CORDITE_UI_NODE_API="https://localhost:8080/api/"
ENV CORDITE_UI_DEFAULT_NOTARY_NAME="OU=Cordite Foundation, O=Notary, L=London, C=GB"
ENV CORDITE_UI_CSL_NAME="CN=csl.cordite.foundation, O=Cordite Society Limited, L=London, C=GB"

ENV PATH /app/node_modules/.bin:$PATH

WORKDIR /app

COPY frontend-react/package.json ./
COPY frontend-react/package-lock.json ./
RUN npm ci --silent
ADD frontend-react .
RUN npm run build

FROM nginx:stable-alpine 
COPY --from=build /app/build /app
COPY ./docker/docker-entrypoint.sh /docker-entrypoint.d/entrypoint.sh
COPY ./docker/nginx-site.conf /etc/nginx/conf.d/default.conf
COPY ./docker/expires.conf /etc/nginx/conf.d/expires.conf
ENV CORDITE_UI_NODE_API="https://localhost:8080/api/"
ENV CORDITE_UI_DEFAULT_NOTARY_NAME="OU=Cordite Foundation, O=Notary, L=London, C=GB"
ENV CORDITE_UI_CSL_NAME="CN=csl.cordite.foundation, O=Cordite Society Limited, L=London, C=GB"
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
