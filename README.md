<!--

      Copyright 2020, Cordite Foundation.

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.

-->

# ![Cordite](https://gitlab.com/cordite/cordite/uploads/37fcd730405f565bca6aa09a453ec865/logo-watermark-50.png) Cordite UI

## What is Cordite?  

Cordite is an open source project creating open source DeFi features for Corda. Cordite continues to make leading-edge features available to the Corda community.

Cordite provides decentralised economic and governance services including:

  + decentralised stores and transfers of value allowing new financial instruments to be created inside the existing regulatory framework. eg. tokens, crypto-coins, digital cash, virtual currency, distributed fees, micro-billing  
  + decentralised forms of governance allowing new digital autonomous organisations to be created using existing legal entities eg. digital mutual societies or digital joint stock companies  
  + decentralised consensus in order to remove the need for a central operator, owner or authority. Allowing Cordite to be more resilient, cheaper, agile and private than incumbent market infrastructure  

Cordite is open source, regulatory friendly, enterprise ready and finance grade.  

Cordite is built on [Corda](http://corda.net), a finance grade distributed ledger technology, meeting the highest standards of the banking industry, yet it is applicable to any commercial scenario. The outcome of over two years of intense research and development by over 80 of the world’s largest financial institutions. 

## Cordite UI

Cordite UI provides a simple single page app (SPA) for Cordite node operators to:
   * join the Cordite DAO
   * vote on Cordite DAO proposals
   * create Cordite accounts
   * transfer tokens between accounts
   * transfer token to accounts on other Cordite nodes

The Cordite UI is intended to be deployed in a client-server pattern, where the Cordite UI is the client/front-end and [a Cordite node](https://gitlab.com/cordite/cordite) is the server/back-end.

## Run Cordite UI

This app requires a Cordite Node to work with. Please refer to the [Cordite README](https://gitlab.com/cordite/cordite/-/blob/master/README.MD#how-do-i-deploy-my-own-cordite-node) to start one.

### Docker Example

This is an example script to start the UI connecting to a node on the Cordite test networks.

```bash
docker run --rm -d --name cordite-ui \
  -p 3000:80 \
  -e CORDITE_UI_NODE_API=https://<cordite-node-host>:<cordite-port>/api/ \
  -e CORDITE_UI_DEFAULT_NOTARY_NAME="OU=Cordite Foundation, O=Notary, L=London, C=GB" \
  cordite/cordite-ui:latest
```

### Running from Source

``` bash
cd frontend-react
npm install

REACT_APP_CORDITE_UI_DEFAULT_NOTARY_NAME="O=Notary, OU=Cordite Foundation, L=London, C=GB" \
REACT_APP_CORDITE_UI_NODE_API="<cordite-node-host>:<cordite-port>/api/" \
npm start

```
### Environment Variables

The application support a number of environment variables. These can be setup either when running the app natively on the command line, or via docker. 

| Command line                             | Docker                         | Default                                                              | Purpose                               |
| ---------------------------------------- | ------------------------------ | -------------------------------------------------------------------- | ------------------------------------- |
| REACT_APP_CORDITE_UI_NODE_API            | CORDITE_UI_NODE_API            | https://localhost:8080/api/                                          | Braid API endpoint                    |
| REACT_APP_CORDITE_UI_DEFAULT_NOTARY_NAME | CORDITE_UI_DEFAULT_NOTARY_NAME | OU=Cordite Foundation, O=Cordite Bootstrap Notary, L=London, C=GB    | Default Corda Notary for transactions |
| REACT_APP_CORDITE_UI_CSL_NAME            | CORDITE_UI_CSL_NAME            | CN=csl.cordite.foundation, O=Cordite Society Limited, L=London, C=GB | Default Cordite Society DAO Party     |


## How do I get in touch?

  * News is announced on [@We_are_Cordite](https://twitter.com/we_are_cordite)
  * More information can be found on [Cordite website](https://cordite.foundation)
  * We use #cordite channel on [Corda slack](https://slack.corda.net/) 
  * We informally meet at the [Corda London meetup](https://www.meetup.com/pro/corda/)

## What if something does not work?
We encourage you to raise any issues/bugs you find in Cordite. Please follow the below steps before raising issues:

   1. Check on the [Issues backlog](https://gitlab.com/cordite/cordite-ui/issues) to make sure an issue on the topic has not already been raised
   2. Post your question on the #cordite channel on [Corda slack](https://slack.corda.net/)
   3. If none of the above help solve the issue, [raise an issue](https://gitlab.com/cordite/cordite-ui/issues/new?issue) following the contributions guide


## How do I contribute

We welcome contributions both technical and non-technical with open arms! There's a lot of work to do here. The [Contributing Guide](https://gitlab.com/cordite/cordite-ui/blob/master/contributing.md) provides more information on how to contribute.



## Who is behind Cordite?

Cordite is being developed by a group of financial services companies, software vendors and open source contributors. The project is hosted here on GitLab. 


## What open source license has this been released under?

All software in this repository is licensed under the Apache License, Version 2.0 (the "License"); you may not use this software except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.

## Build

### Build from source

```
cd frontend-react
npm install
npm run-script build
```

### Start the development server

```bash
npm start
```

### Build the docker image

```
docker build -t "cordite/cordite-ui:local" .
```